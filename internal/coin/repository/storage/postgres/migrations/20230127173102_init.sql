-- +goose Up
-- +goose StatementBegin

CREATE TABLE IF NOT EXISTS event_log
(
    address     VARCHAR(42),
    topics      VARCHAR(66)[],
    data        bytea,
    blockNumber BIGINT,
    txHash      VARCHAR(66)  UNIQUE,
    txIndex     BIGINT,
    blockHash   VARCHAR(66),
    index       BIGINT,
    removed     bool,
    createdAt   timestamp,
    modifiedAt  timestamp
);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin

DROP TABLE IF EXISTS event_log;

-- +goose StatementEnd
