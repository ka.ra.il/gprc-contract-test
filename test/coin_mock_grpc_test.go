package coin

import (
	"context"
	"github.com/golang/mock/gomock"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	coin "gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/coin"
	cn "gitlab.com/ka.ra.il/grpc-contract-test/test/mock_coin"
	"testing"
)

func TestMockService(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	t.Run("test balance", func(t *testing.T) {
		cw := cn.NewMockWalletServiceClient(ctrl)
		cw.EXPECT().Balance(context.Background(), &coin.BalanceRequest{}).Return(&coin.BalanceResponse{}, nil)
		cw.Balance(context.Background(), &coin.BalanceRequest{})
	})

	t.Run("test get max money", func(t *testing.T) {
		cw := cn.NewMockWalletServiceClient(ctrl)
		cw.EXPECT().GetMaxMoney(context.Background(), &empty.Empty{}).Return(&coin.BalanceResponse{}, errors.New(""))
		cw.GetMaxMoney(context.Background(), &empty.Empty{})
	})

	t.Run("test set max money", func(t *testing.T) {
		cw := cn.NewMockWalletServiceClient(ctrl)
		cw.EXPECT().SetMaxMoney(context.Background(), &coin.SetMaxMoneyRequest{}).Return(&coin.ResponseTx{}, errors.New(""))
		cw.SetMaxMoney(context.Background(), &coin.SetMaxMoneyRequest{})
	})

	t.Run("test send", func(t *testing.T) {
		cw := cn.NewMockWalletServiceClient(ctrl)
		cw.EXPECT().Send(context.Background(), &coin.SendRequest{}).Return(&coin.ResponseTx{}, errors.New(""))
		cw.Send(context.Background(), &coin.SendRequest{})
	})
}
