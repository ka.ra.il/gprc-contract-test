package storage

import (
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_subcribe"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"

	"context"
)

type Storage interface {
	EventLog
	EventSubscribe
}

type EventLog interface {
	CreateEventLog(ctx context.Context, event ...*event_log.EventLog) ([]*event_log.EventLog, error)
	UpdateEventLog(ctx context.Context, TxHash string, updateFn func(c *event_log.EventLog) (*event_log.EventLog, error)) (*event_log.EventLog, error)
	DeleteEventLog(ctx context.Context, TxHash string) error
	GetLastBlockNumber(ctx context.Context, addressContract string) (int64, error)
	EventLogReader
}

type EventLogReader interface {
	ListEventLog(ctx context.Context, parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error)
	ReadEventLogByTxHash(ctx context.Context, TxHash string) (response *event_log.EventLog, err error)
	CountEventLog(ctx context.Context) (uint64, error)
}

type EventSubscribe interface {
	CreateEventSubscribe(ctx context.Context, events ...*event_subcribe.EventSubscribe) ([]*event_subcribe.EventSubscribe, error)
	UpdateEventSubscribe(ctx context.Context, TxHash string, updateFn func(c *event_subcribe.EventSubscribe) (*event_subcribe.EventSubscribe, error)) (*event_subcribe.EventSubscribe, error)
	DeleteEventSubscribe(ctx context.Context, TxHash string) error
	EventSubscribeReader
}

type EventSubscribeReader interface {
	ListEventSubscribe(ctx context.Context, parameter queryParameter.QueryParameter) ([]*event_subcribe.EventSubscribe, error)
	ReadEventSubscribeByTxHash(ctx context.Context, TxHash string) (response *event_subcribe.EventSubscribe, err error)
	CountEventSubscribe(ctx context.Context) (uint64, error)
}

type KafkaSaveTopic interface {
	CreateKafkaSaveTopic(ctx context.Context, msg *kafka_save_topic.KafkaSaveTopic) (*kafka_save_topic.KafkaSaveTopic, error)
	ListKafkaSaveTopic(ctx context.Context, parameters queryParameter.QueryParameter) ([]*kafka_save_topic.KafkaSaveTopic, error)
}
