package eth

import (
	"context"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"time"
)

type WaitTx struct {
	Ctx       context.Context
	Client    *ethclient.Client
	Hash      common.Hash
	DurTimer  time.Duration
	DurTicker time.Duration
}

func WaitTxConfirmed(w WaitTx) *types.Transaction {
	ticker := time.NewTicker(w.DurTicker)
	timer := time.NewTimer(w.DurTimer)
	ch := make(chan *types.Transaction)

	go func() {
		for {
			select {
			case <-ticker.C:
				tx, pending, _ := w.Client.TransactionByHash(w.Ctx, w.Hash)
				if !pending {
					ch <- tx
					return
				}
			case <-timer.C:
				ch <- nil
				return
			}
		}
	}()

	res := <-ch
	ticker.Stop()

	return res
}

func SliceHashToSting(topics []common.Hash) []string {
	res := make([]string, len(topics))
	for _, v := range topics {
		res = append(res, v.String())
	}
	return res
}

func WaitForReceipt(ctx context.Context, c *ethclient.Client, hash common.Hash, addr common.Address) (*types.Receipt, error) {
	query := ethereum.FilterQuery{
		Addresses: []common.Address{addr},
	}
	var ch = make(chan types.Log)
	sub, err := c.SubscribeFilterLogs(ctx, query, ch)
	if err != nil {
		return nil, err
	}

	for confirmed := false; !confirmed; {
		select {
		case err := <-sub.Err():
			return nil, err
		case vLog := <-ch:
			if vLog.TxHash == hash {
				confirmed = true
			}
		}

	}
	return c.TransactionReceipt(ctx, hash)
}
