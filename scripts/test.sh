#!/bin/bash
set -e
readonly testFile="$1"
cd test/
go test -v $testFile