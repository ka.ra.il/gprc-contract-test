package kafka_save_topic

import (
	"github.com/jackc/pgtype"
	"time"
)

type KafkaSaveTopic struct {
	topic     string
	eventData pgtype.JSONB
	createdAt time.Time
}

func (k KafkaSaveTopic) Topic() string {
	return k.topic
}

func (k KafkaSaveTopic) EventData() pgtype.JSONB {
	return k.eventData
}

func (k KafkaSaveTopic) CreatedAt() time.Time {
	return k.createdAt
}

func New(topic string, eventData pgtype.JSONB, createdAt time.Time) (*KafkaSaveTopic, error) {
	return &KafkaSaveTopic{
		topic:     topic,
		eventData: eventData,
		createdAt: createdAt,
	}, nil
}
