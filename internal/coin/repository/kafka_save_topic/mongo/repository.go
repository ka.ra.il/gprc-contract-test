package mongo

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

type Repository struct {
	db  *mongo.Database
	log *logrus.Entry
}

func New(c config.MongoConfig, log *logrus.Entry) (*Repository, error) {
	//mongoUrl := fmt.Sprintf("mongodb://%s:%s@%s:%d/%s?ssl=false&authSource=admin", c.UserName, c.Password, c.Host, c.Port, c.DbName)
	//fmt.Println("mongoUrl", mongoUrl)
	credential := options.Credential{
		Username: c.RootUserName,
		Password: c.RootPassword,
		//Username: c.UserName,
		//Password: c.Password,
	}
	clientOpts := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%d/%s", c.Host, c.Port, c.DbName)).SetAuth(credential)
	fmt.Println(clientOpts.GetURI())
	client, err := mongo.Connect(context.TODO(), clientOpts)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}
	return &Repository{
		db:  client.Database(c.DbName),
		log: log,
	}, nil
}

func (r Repository) Close(ctx context.Context) error {
	return r.db.Client().Disconnect(ctx)
}
