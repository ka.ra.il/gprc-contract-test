package main

import (
	"context"
	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/sirupsen/logrus"
	config2 "gitlab.com/ka.ra.il/grpc-contract-test/config"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event/kafka/consumer"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event/kafka/producer"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/kafka_save_topic/mongo"
	repositoryStorage "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/storage/postgres"
	grpc2 "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/server/grpc"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/server/workers"
	useCaseEventLog "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/event_log"
	useCaseKafkaSaveTopic "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/kafka_save_topic"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/common/server"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/store/postgres"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/tracing"
	"google.golang.org/grpc"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	log := logrus.NewEntry(logrus.StandardLogger())

	c := config2.Config{}
	err := config2.NewConfig(&c, "./../../../")
	if err != nil {
		log.Fatalf("ошибка конфигурации: %s", err)
	}
	store, err := c.NewStore()
	if err != nil {
		log.Fatalf("ошибка при создании store: %s", err)
	}

	conn, err := postgres.New(postgres.Settings{
		Host:     c.PostgresConfig.Host,
		Port:     c.PostgresConfig.Port,
		Database: c.PostgresConfig.Name,
		User:     c.PostgresConfig.User,
		Password: c.PostgresConfig.Password,
		SSLMode:  c.PostgresConfig.SSLMode,
	})
	if err != nil {
		panic(err)
	}
	defer conn.Pool.Close()

	closer, err := tracing.New(context.Background(), c.JaegerConfig, c.ServiceName)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = closer.Close(); err != nil {
			log.Error(err)
		}
	}()

	// repoEventLog, err:= repositoryEventLog.New(conn.Pool, repositoryContact.Options{})
	// 	if err != nil {
	//	log.Fatalf("ошибка при создании repoEventLog: %s", err)
	// }

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	repoPostgres, err := repositoryStorage.New(conn.Pool, repositoryStorage.Options{}, log, repositoryStorage.Migration{Dir: c.PostgresConfig.MigrationDir, IsMigrate: true})
	if err != nil {
		log.Fatalf("ошибка при создании postgres pool: %s", err)
	}

	repoMongo, err := mongo.New(c.MongoConfig, log)
	if err != nil {
		log.Fatalf("ошибка при создании mongo: %s", err)
	}
	defer repoMongo.Close(ctx)

	useCaseEventLog := useCaseEventLog.New(repoPostgres, useCaseEventLog.Options{})

	useCaseKafkaSaveTopic := useCaseKafkaSaveTopic.New(repoMongo, useCaseKafkaSaveTopic.Options{})

	go consumer.NewSubscribeRealtimePublisher(consumer.ConfigConsumer{
		Topic:                c.KafkaConfig.TopicSuccessTx,
		Brokers:              c.KafkaConfig.Brokers,
		IsSaveRepository:     true,
		IsSendCounterTopic:   true,
		Storage:              useCaseKafkaSaveTopic,
		IsSimulateErrorPanic: false,
		Marshaler:            kafka.DefaultMarshaler{},
		Unmarshaler:          kafka.DefaultMarshaler{},
	})

	//schemaRegistryClient := srclient.CreateSchemaRegistryClient("http://127.0.0.1:8081")
	//go consumer.NewSubscribeNoPublisher(consumer.ConfigConsumer{
	//	//Topic:         "db_.public.event_log",
	//	Topic:         "event_log",
	//	Brokers:       c.KafkaConfig.Brokers,
	//	Unmarshaler:   consumer.AvroMarshaler{schemaRegistryClient},
	//	Marshaler:     kafka.DefaultMarshaler{},
	//	ConsumerGroup: "avro",
	//})

	//protobufResolver := protobuf_lib.NewSchemaRegistryProtobufResolver(*schemaRegistryClient, protoregistry.GlobalTypes, protobuf_lib.ValueDeserialization)
	//deserializer := protobuf_lib.NewProtobufDeserializer(protobufResolver)

	go consumer.NewSubscribeNoPublisher(consumer.ConfigConsumer{
		//Topic:         "db_.public.event_log",
		Topic:         "dbprotobuf_event_log",
		Brokers:       c.KafkaConfig.Brokers,
		Unmarshaler:   consumer.NewProtobufMarshaller2("http://127.0.0.1:8081", "dbprotobuf_event_log"),
		Marshaler:     kafka.DefaultMarshaler{},
		ConsumerGroup: "protobuf",
	})

	//go workers.SubscribeEvent(ctx, log, useCaseEventLog, store.UrlWs, store.ContractAddress)

	go workers.WorkerEventLog(&workers.EventLogWorker{
		Ctx:      ctx,
		Log:      log,
		UseCase:  useCaseEventLog,
		Url:      store.UrlHttp,
		Addr:     store.ContractAddress,
		Interval: time.Duration(c.WorkerEventLogWaitTimeout) * time.Second,
		ConfigProducer: producer.ConfigKafka{
			Topic:   c.KafkaConfig.TopicSuccessTx,
			Brokers: c.KafkaConfig.Brokers,
		},
	})

	server.RunGRPCServer(ctx, func(server *grpc.Server) {
		svc, err := grpc2.NewGrpcServer(store.UrlHttp, store.StoreSession)
		if err != nil {
			return
		}
		coin.RegisterWalletServiceServer(server, svc)
	})

}
