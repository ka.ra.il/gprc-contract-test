package event_log

import (
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/jackc/pgtype"
	"github.com/sirupsen/logrus"
	eventPackage "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/eth"
	"time"
)

func ConvertEthLogToEventLog(contractAbi *abi.ABI, eventLogHash *eventPackage.LogEventTypeHash, Elogs []types.Log, log *logrus.Entry) ([]*EventLog, error) {
	res := make([]*EventLog, 0)
	if len(Elogs) == 0 {
		return res, nil
	}
	for _, v := range Elogs {
		evenType, jsonb, err := ReadEvent(contractAbi, eventLogHash, v)
		if err != nil {
			log.Errorf("ReadEvent %s", err)
		}
		timeAt := time.Now().UTC()
		item, _ := New(
			v.Address.String(),
			eth.SliceHashToSting(v.Topics),
			v.Data,
			v.BlockNumber,
			v.TxHash.String(),
			uint64(v.TxIndex),
			v.BlockHash.String(),
			uint64(v.Index),
			v.Removed,
			timeAt,
			timeAt,
			evenType,
			jsonb,
		)
		res = append(res, item)
	}
	return res, nil
}

func ReadEvent(contractAbi *abi.ABI, eventLog *eventPackage.LogEventTypeHash, vLog types.Log) (string, pgtype.JSONB, error) {
	jsonb := pgtype.JSONB{}
	jsonb.UnmarshalJSON([]byte("{}"))
	switch vLog.Topics[0].Hex() {
	case eventLog.Sent:
		var event eventPackage.LogSend
		eventName := "Sent"
		err := contractAbi.UnpackIntoInterface(&event, eventName, vLog.Data)
		if err != nil {
			return "", jsonb, errorWrap(err, eventName)
		}
		err = jsonb.Set(event)
		return eventName, jsonb, errorWrap(err, eventName)
	case eventLog.Mint:
		var event eventPackage.Mint
		eventName := "Mint"
		err := contractAbi.UnpackIntoInterface(&event, eventName, vLog.Data)
		if err != nil {
			return "", jsonb, errorWrap(err, eventName)
		}
		err = jsonb.Set(event)
		return eventName, jsonb, errorWrap(err, eventName)
	case eventLog.NewMaxMoney:
		var event eventPackage.NewMaxMoney
		eventName := "NewMaxMoney"
		err := contractAbi.UnpackIntoInterface(&event, eventName, vLog.Data)
		if err != nil {
			return "", jsonb, errorWrap(err, eventName)
		}
		err = jsonb.Set(event)

		return eventName, jsonb, errorWrap(err, eventName)
	case eventLog.NewAccount:
		var event eventPackage.NewAccount
		eventName := "NewAccount"
		err := contractAbi.UnpackIntoInterface(&event, eventName, vLog.Data)
		if err != nil {
			return "", jsonb, errorWrap(err, eventName)
		}
		err = jsonb.Set(event)
		return eventName, jsonb, errorWrap(err, eventName)
	case eventLog.ChangeFlagProcessing:
		var event eventPackage.ChangeFlagProcessing
		eventName := "ChangeFlagProcessing"
		err := contractAbi.UnpackIntoInterface(&event, eventName, vLog.Data)
		if err != nil {
			return "", jsonb, errorWrap(err, eventName)
		}
		err = jsonb.Set(event)
		return eventName, jsonb, errorWrap(err, eventName)
	}
	return "", jsonb, nil
}

func errorWrap(err error, eventName string) error {
	if err != nil {
		err = fmt.Errorf("%s: %s", eventName, err)
	}
	return err
}
