package postgres

import (
	"context"
	"errors"
	"github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	dao "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event_log/postgres/dao"
	"strings"

	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/transaction"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/columnCode"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
)

var mappingSort = map[columnCode.ColumnCode]string{
	"id":          "id",
	"name":        "name",
	"description": "description",
}

func (r *Repository) CreateEventLog(ctx context.Context, eventLogs ...*event_log.EventLog) ([]*event_log.EventLog, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.CreateEventTx(ctx, tx, eventLogs...)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) CreateEventTx(ctx context.Context, tx pgx.Tx, eventLogs ...*event_log.EventLog) ([]*event_log.EventLog, error) {
	if len(eventLogs) == 0 {
		return []*event_log.EventLog{}, nil
	}
	queryInsert := r.genSQL.Insert("event_log").
		Columns(strings.Join(dao.CreateColumnEventLog, ","))
	for _, log := range eventLogs {
		queryInsert = queryInsert.Values(
			log.Address(),
			log.Topics(),
			log.Data(),
			log.BlockNumber(),
			log.TxHash(),
			log.TxIndex(),
			log.BlockHash(),
			log.Index(),
			log.Removed(),
			log.CreatedAt(),
			log.ModifiedAt(),
			log.EventType(),
			log.EventData(),
		)
	}
	query, args, err := queryInsert.Suffix("ON CONFLICT DO NOTHING").ToSql()
	if err != nil {
		return nil, err
	}

	if _, errEx := tx.Exec(ctx, query, args...); errEx != nil {
		return nil, errEx
	}

	return eventLogs, nil
}

func (r *Repository) UpdateEventLog(ctx context.Context, TxHash string, updateFn func(eventLog *event_log.EventLog) (*event_log.EventLog, error)) (*event_log.EventLog, error) {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	upEventLog, err := r.oneEventLogTx(ctx, tx, TxHash)
	if err != nil {
		return nil, err
	}
	eventLogForUpdate, err := updateFn(upEventLog)
	if err != nil {
		return nil, err
	}

	query, args, err := r.genSQL.Update("event_log").
		Set("address", eventLogForUpdate.Address()).
		Set("topics", eventLogForUpdate.Topics()).
		Set("data", eventLogForUpdate.TxHash()).
		Where(squirrel.And{
			squirrel.Eq{
				"txhash": TxHash,
			},
		}).
		Suffix(`RETURNING
			id,
			name,
			description,
			created_at,
			modified_at`,
		).
		ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var daoEventLog []*dao.EventLog
	if err = pgxscan.ScanAll(&daoEventLog, rows); err != nil {
		return nil, err
	}

	return eventLogForUpdate, nil
}

func (r *Repository) DeleteEventLog(ctx context.Context, TxHash string) error {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	if err = r.deleteEventLogTx(ctx, tx, TxHash); err != nil {
		return err
	}

	return nil
}

func (r *Repository) deleteEventLogTx(ctx context.Context, tx pgx.Tx, TxHash string) error {
	query, args, err := r.genSQL.
		Delete("event_log").
		Where(squirrel.Eq{"id": TxHash}).
		ToSql()

	if err != nil {
		return err
	}

	if _, errEx := tx.Exec(ctx, query, args...); errEx != nil {
		return err
	}

	return nil
}

func (r *Repository) ListEventLog(ctx context.Context, parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error) {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.listEventLogTx(ctx, tx, parameter)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) listEventLogTx(ctx context.Context, tx pgx.Tx, parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error) {
	var result []*event_log.EventLog

	var builder = r.genSQL.Select(
		"address",
		"topics",
		"data",
		"blocknumber",
		"txhash",
		"txindex",
		"blockhash",
		"index",
		"removed",
		"createdat",
		"modifiedat",
	).
		From("event_log")

	if len(parameter.Sorts) > 0 {
		builder = builder.OrderBy(parameter.Sorts.Parsing(mappingSort)...)
	} else {
		builder = builder.OrderBy("createdat DESC")
	}

	if parameter.Pagination.Limit > 0 {
		builder = builder.Limit(parameter.Pagination.Limit)
	}
	if parameter.Pagination.Offset > 0 {
		builder = builder.Offset(parameter.Pagination.Offset)
	}

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var events []*dao.EventLog
	if err = pgxscan.ScanAll(&events, rows); err != nil {
		return nil, err
	}

	for _, g := range events {
		domainEventLog, err := g.ToDomainEventLog()
		if err != nil {
			return nil, err
		}
		result = append(result, domainEventLog)
	}
	return result, nil
}

func (r *Repository) ReadEventLogByTxHash(ctx context.Context, TxHash string) (*event_log.EventLog, error) {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.oneEventLogTx(ctx, tx, TxHash)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) oneEventLogTx(ctx context.Context, tx pgx.Tx, TxHash string) (response *event_log.EventLog, err error) {

	var builder = r.genSQL.Select(
		"address",
		"topics",
		"data",
		"blocknumber",
		"txhash",
		"txindex",
		"blockhash",
		"index",
		"removed",
		"createdat",
		"modifiedat",
		"event_type",
		"event_data",
	).
		From("event_log").Where(squirrel.Eq{"txhash": TxHash}).Limit(1)

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var daoEventLog []*dao.EventLog
	if err = pgxscan.ScanAll(&daoEventLog, rows); err != nil {
		return nil, err
	}

	if len(daoEventLog) == 0 {
		return nil, errors.New("eventLog not found")
	}
	return daoEventLog[0].ToDomainEventLog()

}

func (r *Repository) CountEventLog(ctx context.Context) (uint64, error) {

	var builder = r.genSQL.Select(
		"COUNT(id)",
	).From("event_log")

	query, args, err := builder.ToSql()
	if err != nil {
		return 0, err
	}

	row := r.db.QueryRow(ctx, query, args...)
	var total uint64

	if err = row.Scan(&total); err != nil {
		return 0, err
	}

	return total, nil
}

func (r *Repository) GetLastBlockNumber(ctx context.Context, addressContract string) (int64, error) {
	var builder = r.genSQL.Select(
		"blocknumber",
	).
		From("event_log").
		Where(squirrel.Eq{"address": addressContract}).
		OrderBy("createdat desc").
		Limit(1)

	query, args, err := builder.ToSql()
	if err != nil {
		return 0, err
	}
	row := r.db.QueryRow(ctx, query, args...)
	var blockNumber int64
	err = row.Scan(&blockNumber)
	if err != nil {
		if err == pgx.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	return blockNumber, nil
}

func (r *Repository) ReadEventLogByParam(ctx context.Context, params event_log.EventLogParams) (*event_log.EventLog, error) {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.oneEventLogByParams(ctx, tx, params)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) oneEventLogByParams(ctx context.Context, tx pgx.Tx, e event_log.EventLogParams) (response *event_log.EventLog, err error) {

	var builder = r.genSQL.Select(
		"address",
		"topics",
		"data",
		"blocknumber",
		"txhash",
		"txindex",
		"blockhash",
		"index",
		"removed",
		"createdat",
		"modifiedat",
		"event_type",
		"event_data",
	).
		From("event_log")

	if e.Address() != "" {
		builder = builder.Where(squirrel.Eq{"address": e.Address()})
	}

	if len(e.Topics()) != 0 {
		builder = builder.Where(squirrel.Eq{"topics": e.Topics()})
	}

	if e.BlockHash() != "" {
		builder = builder.Where(squirrel.Eq{"blockhash": e.BlockHash()})
	}

	if e.BlockNumber() != 0 {
		builder = builder.Where(squirrel.Eq{"blocknumber": e.BlockNumber()})
	}

	if e.TxHash() != "" {
		builder = builder.Where(squirrel.Eq{"txhash": e.TxHash()})
	}

	if e.EventType() != "" {
		builder = builder.Where(squirrel.Eq{"event_type": e.EventType()})
	}

	if e.EventData().Status == 2 {
		builder = builder.Where(squirrel.Eq{"event_data": e.EventData()})
	}

	builder = builder.Limit(1)

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var daoEventLog []*dao.EventLog
	if err = pgxscan.ScanAll(&daoEventLog, rows); err != nil {
		return nil, err
	}

	if len(daoEventLog) == 0 {
		return nil, errors.New("eventLog not found")
	}
	return daoEventLog[0].ToDomainEventLog()

}
