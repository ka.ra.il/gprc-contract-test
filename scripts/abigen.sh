#!/bin/bash
set -e

readonly fileName="$1"
pkg=$(echo "$1" | tr '[:upper:]' '[:lower:]')

cd contracts
CWD=$(pwd)
solc --abi --overwrite "$CWD/$fileName.sol" -o build
solc --bin --overwrite "$CWD/$fileName.sol" -o build
abigen --abi "$CWD/build/$fileName.abi" --pkg "$pkg" --type Storage --out "$pkg/$fileName.go" --bin "$CWD/build/$fileName.bin"

