package converter

import (
	"math/rand"
	"strings"
	"time"
)

func RandomString(n int) string {
	rand.Seed(time.Now().UnixNano())
	charset := "abcdefghijklmnopqrstxwyzABCDEFGHIJKLMNOQRSTXWYZ"

	b := strings.Builder{}
	b.Grow(n)
	for i := 0; i < n; i++ {
		b.WriteByte(charset[rand.Intn(len(charset))])
	}
	return b.String()
}
