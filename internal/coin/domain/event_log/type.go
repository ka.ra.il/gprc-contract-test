package event_log

import (
	"encoding/json"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/jackc/pgtype"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/eth"
	"time"
)

type EventLog struct {
	address     string       `json:"address"`
	topics      []string     `json:"topics"`
	data        []byte       `json:"data"`
	blockNumber uint64       `json:"blockNumber"`
	txHash      string       `json:"txHash"`
	txIndex     uint64       `json:"txIndex"`
	blockHash   string       `json:"blockHash"`
	index       uint64       `json:"index"`
	removed     bool         `json:"removed"`
	createdAt   time.Time    `json:"createdAt"`
	modifiedAt  time.Time    `json:"modifiedAt"`
	eventType   string       `json:"eventType"`
	eventData   pgtype.JSONB `json:"eventData"`
}

func (e EventLog) name() {

}

func (e EventLog) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Address     string       `json:"address"`
		Topics      []string     `json:"topicsHash"`
		Data        []byte       `json:"data"`
		BlockNumber uint64       `json:"blockNumber"`
		TxHash      string       `json:"txHash"`
		TxIndex     uint64       `json:"txIndex"`
		BlockHash   string       `json:"blockHash"`
		Index       uint64       `json:"index"`
		Removed     bool         `json:"removed"`
		CreatedAt   time.Time    `json:"createdAt"`
		ModifiedAt  time.Time    `json:"modifiedAt"`
		EventType   string       `json:"eventType"`
		EventData   pgtype.JSONB `json:"eventData"`
	}{
		Address:     e.Address(),
		Topics:      e.Topics(),
		Data:        e.Data(),
		BlockNumber: e.BlockNumber(),
		TxHash:      e.TxHash(),
		TxIndex:     e.TxIndex(),
		BlockHash:   e.BlockHash(),
		Index:       e.Index(),
		Removed:     e.Removed(),
		CreatedAt:   e.CreatedAt(),
		ModifiedAt:  e.ModifiedAt(),
		EventType:   e.EventType(),
		EventData:   e.EventData(),
	})
}

func NewByVlog(v types.Log) *EventLog {
	return &EventLog{
		address:     v.Address.String(),
		topics:      eth.SliceHashToSting(v.Topics),
		data:        v.Data,
		blockNumber: v.BlockNumber,
		txHash:      v.TxHash.String(),
		txIndex:     uint64(v.TxIndex),
		blockHash:   v.BlockHash.String(),
		index:       uint64(v.Index),
		removed:     v.Removed,
		createdAt:   time.Now().UTC(),
	}
}

func New(address string, topics []string, data []byte, blockNumber uint64, txHash string, txIndex uint64, blockHash string, index uint64, removed bool, createdAt, modifiedAt time.Time, evenType string, eventData pgtype.JSONB) (*EventLog, error) {
	return &EventLog{
		address:     address,
		topics:      topics,
		data:        data,
		blockNumber: blockNumber,
		txHash:      txHash,
		txIndex:     txIndex,
		blockHash:   blockHash,
		index:       index,
		removed:     removed,
		createdAt:   createdAt,
		modifiedAt:  modifiedAt,
		eventType:   evenType,
		eventData:   eventData,
	}, nil
}

func (e EventLog) Address() string {
	return e.address
}

func (e EventLog) Data() []byte {
	if e.data == nil {
		return make([]byte, 0)
	}
	return e.data
}

func (e EventLog) BlockNumber() uint64 {
	return e.blockNumber
}

func (e EventLog) TxHash() string {
	return e.txHash
}

func (e EventLog) TxIndex() uint64 {
	return e.txIndex
}

func (e EventLog) BlockHash() string {
	return e.blockHash
}

func (e EventLog) Index() uint64 {
	return e.index
}

func (e EventLog) Removed() bool {
	return e.removed
}

func (e EventLog) Topics() []string {
	return e.topics
}

func (e EventLog) CreatedAt() time.Time {
	return e.createdAt
}

func (e EventLog) ModifiedAt() time.Time {
	return e.modifiedAt
}

func (e EventLog) EventType() string {
	return e.eventType
}

func (e EventLog) EventData() pgtype.JSONB {
	return e.eventData
}

type EventLogParams struct {
	address     string
	topics      []string
	blockNumber uint64
	txHash      string
	blockHash   string
	eventType   string
	eventData   pgtype.JSONB
}

func (e *EventLogParams) SetAddress(address string) {
	e.address = address
}

func (e *EventLogParams) SetTopics(topics []string) {
	e.topics = topics
}

func (e *EventLogParams) SetBlockNumber(blockNumber uint64) {
	e.blockNumber = blockNumber
}

func (e *EventLogParams) SetTxHash(txHash string) {
	e.txHash = txHash
}

func (e *EventLogParams) SetBlockHash(blockHash string) {
	e.blockHash = blockHash
}

func (e *EventLogParams) SetEventType(eventType string) {
	e.eventType = eventType
}

func (e *EventLogParams) SetEventData(eventData pgtype.JSONB) {
	e.eventData = eventData
}

func (e EventLogParams) Address() string {
	return e.address
}

func (e EventLogParams) Topics() []string {
	return e.topics
}

func (e EventLogParams) BlockNumber() uint64 {
	return e.blockNumber
}

func (e EventLogParams) TxHash() string {
	return e.txHash
}

func (e EventLogParams) BlockHash() string {
	return e.blockHash
}

func (e EventLogParams) EventType() string {
	return e.eventType
}

func (e EventLogParams) EventData() pgtype.JSONB {
	return e.eventData
}
