package consumer

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"sync/atomic"
	"time"

	"github.com/pkg/errors"

	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/ThreeDotsLabs/watermill/message/router/plugin"
)

type ConfigConsumer struct {
	Topic                string
	Brokers              []string
	ConsumerGroup        string
	IsSendCounterTopic   bool
	IsSaveRepository     bool
	IsSimulateErrorPanic bool
	Storage              ConsumerSaveInterface
	Marshaler            kafka.Marshaler
	Unmarshaler          kafka.Unmarshaler
}

type ConsumerSaveInterface interface {
	SaveMsg(message *message.Message) error
}

func NewSubscribeRealtimePublisher(conf ConfigConsumer) error {
	logger := watermill.NewStdLogger(false, false)
	logger.Info("Starting the consumer", nil)

	pub, err := kafka.NewPublisher(
		kafka.PublisherConfig{
			Brokers:   conf.Brokers,
			Marshaler: conf.Marshaler,
		},
		logger,
	)
	if err != nil {
		log.Error(err)
		return err
	}

	r, err := message.NewRouter(message.RouterConfig{}, logger)
	if err != nil {
		log.Error(err)
		return err
	}

	retryMiddleware := middleware.Retry{
		MaxRetries:      1,
		InitialInterval: time.Millisecond * 10,
	}

	poisonQueue, err := middleware.PoisonQueue(pub, fmt.Sprintf("%s_poison_queue", conf.Topic))
	if err != nil {
		log.Error(err)
		return err
	}

	r.AddMiddleware(
		// Recoverer middleware recovers panic from handlers and middlewares
		middleware.Recoverer,

		// Limit incoming messages to 10 per second
		middleware.NewThrottle(10, time.Second).Middleware,

		// If the retries limit is exceeded (see retryMiddleware below), the message is sent
		// to the poison queue (published to poison_queue topic)
		poisonQueue,

		// Retry middleware retries message processing if an error occurred in the handler
		retryMiddleware.Middleware,

		// Correlation ID middleware adds the correlation ID of the consumed message to each produced message.
		// It's useful for debugging.
		middleware.CorrelationID,
	)

	if conf.IsSimulateErrorPanic {
		r.AddMiddleware(
			// Simulate errors or panics from handler
			middleware.RandomFail(0.01),
			middleware.RandomPanic(0.01),
		)
	}

	// Close the router when a SIGTERM is sent
	r.AddPlugin(plugin.SignalsHandler)

	if conf.IsSendCounterTopic {
		subCount, err := createSubscriber(conf.Brokers, fmt.Sprintf("%ss_counter", conf.Topic), logger, conf.Unmarshaler)
		if err != nil {
			log.Error(err)
			return err
		}
		r.AddHandler(
			fmt.Sprintf("%s_counter", conf.Topic),
			conf.Topic,
			subCount,
			fmt.Sprintf("%ss_count", conf.Topic),
			pub,
			Counter{memoryCountStorage{new(int64)}}.Count,
		)
	}

	if conf.IsSaveRepository {
		subImplement, err := createSubscriber(conf.Brokers, fmt.Sprintf("%s_generator", conf.Topic), logger, conf.Unmarshaler)
		if err != nil {
			log.Error(err)
			return err
		}
		r.AddNoPublisherHandler(
			fmt.Sprintf("%s_generator", conf.Topic),
			conf.Topic,
			subImplement,
			conf.Storage.SaveMsg,
		)
	}

	if err = r.Run(context.Background()); err != nil {
		log.Error(err)
		return err
	}
	return nil
}

func NewSubscribeNoPublisher(conf ConfigConsumer) error {
	logger := watermill.NewStdLogger(false, false)
	logger.Info("Starting the consumer", nil)

	pub, err := kafka.NewPublisher(
		kafka.PublisherConfig{
			Brokers:   conf.Brokers,
			Marshaler: conf.Marshaler,
		},
		logger,
	)
	if err != nil {
		log.Error(err)
		return err
	}

	r, err := message.NewRouter(message.RouterConfig{}, logger)
	if err != nil {
		log.Error(err)
		return err
	}

	retryMiddleware := middleware.Retry{
		MaxRetries:      1,
		InitialInterval: time.Millisecond * 10,
	}

	poisonQueue, err := middleware.PoisonQueue(pub, fmt.Sprintf("%s_poison_queue", conf.Topic))
	if err != nil {
		log.Error(err)
		return err
	}

	r.AddMiddleware(
		// Recoverer middleware recovers panic from handlers and middlewares
		middleware.Recoverer,

		// Limit incoming messages to 10 per second
		middleware.NewThrottle(10, time.Second).Middleware,

		// If the retries limit is exceeded (see retryMiddleware below), the message is sent
		// to the poison queue (published to poison_queue topic)
		poisonQueue,

		// Retry middleware retries message processing if an error occurred in the handler
		retryMiddleware.Middleware,

		// Correlation ID middleware adds the correlation ID of the consumed message to each produced message.
		// It's useful for debugging.
		middleware.CorrelationID,
	)

	if conf.IsSimulateErrorPanic {
		r.AddMiddleware(
			// Simulate errors or panics from handler
			middleware.RandomFail(0.01),
			middleware.RandomPanic(0.01),
		)
	}

	// Close the router when a SIGTERM is sent
	r.AddPlugin(plugin.SignalsHandler)

	r.AddSubscriberDecorators()
	subscriber, err := createSubscriber(conf.Brokers, conf.ConsumerGroup, logger, conf.Unmarshaler)
	if err != nil {
		log.Error(err)
		return err
	}
	r.AddNoPublisherHandler(
		fmt.Sprintf("%s_no_publihser", conf.Topic),
		conf.Topic,
		subscriber,
		printMessages,
	)

	if err = r.Run(context.Background()); err != nil {
		log.Error(err)
		return err
	}
	return nil
}

func createSubscriber(brokers []string, consumerGroup string, logger watermill.LoggerAdapter, unmarshaler kafka.Unmarshaler) (message.Subscriber, error) {
	return kafka.NewSubscriber(
		kafka.SubscriberConfig{
			Brokers:       brokers,
			Unmarshaler:   unmarshaler,
			ConsumerGroup: consumerGroup,
		},
		logger,
	)
}

type countUpdated struct {
	NewCount int64 `json:"new_count"`
}

type countStorage interface {
	CountAdd() (int64, error)
	Count() (int64, error)
}

type memoryCountStorage struct {
	count *int64
}

func (m memoryCountStorage) CountAdd() (int64, error) {
	return atomic.AddInt64(m.count, 1), nil
}

func (m memoryCountStorage) Count() (int64, error) {
	return atomic.LoadInt64(m.count), nil
}

type Counter struct {
	countStorage countStorage
}

func (c Counter) Count(msg *message.Message) ([]*message.Message, error) {
	// When implementing counter for production use, you'd probably need to add some kind of deduplication here,
	// unless the used Pub/Sub supports exactly-once delivery.

	newCount, err := c.countStorage.CountAdd()
	if err != nil {
		return nil, errors.Wrap(err, "cannot add count")
	}

	producedMsg := countUpdated{NewCount: newCount}
	b, err := json.Marshal(producedMsg)
	if err != nil {
		return nil, err
	}

	return []*message.Message{message.NewMessage(watermill.NewUUID(), b)}, nil
}

type Test struct {
	After struct {
		//Log dao.EventLog `json:"db_.public.event_log.Value"`
		Log map[string]interface{} `json:"db_.public.event_log.Value"`
	} `json:"after"`
}

func printMessages(msg *message.Message) error {
	fmt.Println("printMessages")
	return nil
}
