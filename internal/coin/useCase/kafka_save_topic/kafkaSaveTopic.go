package kafka_save_topic

import (
	"context"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/jackc/pgtype"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
	"time"
)

func (uc *UseCase) Create(ctx context.Context, msg *kafka_save_topic.KafkaSaveTopic) (*kafka_save_topic.KafkaSaveTopic, error) {
	return uc.adapterStorage.CreateKafkaSaveTopic(ctx, msg)
}

func (uc *UseCase) List(c context.Context, parameter queryParameter.QueryParameter) ([]*kafka_save_topic.KafkaSaveTopic, error) {

	span, ctx := opentracing.StartSpanFromContext(c, "List")
	defer span.Finish()

	return uc.adapterStorage.ListKafkaSaveTopic(ctx, parameter)
}

func (uc *UseCase) SaveMsg(message *message.Message) error {
	var jsonb pgtype.JSONB
	err := jsonb.UnmarshalJSON(message.Payload)
	if err != nil {
		return err
	}
	msg, err := kafka_save_topic.New("", jsonb, time.Now().UTC())
	if err != nil {
		return err
	}
	_, err = uc.Create(context.Background(), msg)
	return err
}
