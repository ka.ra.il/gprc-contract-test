set -e

mongosh -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD <<EOF
db = db.getSiblingDB('$MONGO_DB_NAME')

db.createUser({
  user: '$MONGO_USER_NAME',
  pwd: '$MONGO_PASSWORD',
  roles: [{ role: 'readWrite', db: '$MONGO_DB_NAME' }],
});

db.createCollection('kafkaSaveTopic')

EOF