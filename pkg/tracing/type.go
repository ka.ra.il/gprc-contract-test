package tracing

import (
	"context"
	"fmt"
	"github.com/uber/jaeger-client-go"
	cnf "gitlab.com/ka.ra.il/grpc-contract-test/config"
	"io"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go/config"
)

func New(ctx context.Context, conf cnf.JaegerConfig, serviceName string) (io.Closer, error) {
	fmt.Printf("%s:%d", conf.Host, conf.Port)
	cfg := &config.Configuration{
		ServiceName: serviceName,
		RPCMetrics:  true,
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:           false,
			LocalAgentHostPort: fmt.Sprintf("%s:%d", conf.Host, conf.Port),
		},
	}

	tracer, closer, err := cfg.NewTracer(config.Logger(jaeger.StdLogger))
	if err != nil {
		return nil, err
	}

	opentracing.SetGlobalTracer(tracer)

	return closer, nil
}
