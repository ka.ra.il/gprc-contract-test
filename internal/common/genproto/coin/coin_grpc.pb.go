// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: coin.proto

package coin

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// WalletServiceClient is the client API for WalletService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type WalletServiceClient interface {
	Mint(ctx context.Context, in *MintRequest, opts ...grpc.CallOption) (*ResponseTx, error)
	Send(ctx context.Context, in *SendRequest, opts ...grpc.CallOption) (*ResponseTx, error)
	NewAccount(ctx context.Context, in *NewAccRequest, opts ...grpc.CallOption) (*ResponseTx, error)
	Balance(ctx context.Context, in *BalanceRequest, opts ...grpc.CallOption) (*BalanceResponse, error)
	SetMaxMoney(ctx context.Context, in *SetMaxMoneyRequest, opts ...grpc.CallOption) (*ResponseTx, error)
	GetMaxMoney(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*BalanceResponse, error)
	SetStatusProcessing(ctx context.Context, in *StatusProcessing, opts ...grpc.CallOption) (*ResponseTx, error)
	GetBalancesList(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*ListBalances, error)
	Wallet(ctx context.Context, in *WalletRequest, opts ...grpc.CallOption) (*WalletResponse, error)
	GetWalletList(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*ListWalletOwner, error)
	GetFields(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*FieldsResponse, error)
}

type walletServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewWalletServiceClient(cc grpc.ClientConnInterface) WalletServiceClient {
	return &walletServiceClient{cc}
}

func (c *walletServiceClient) Mint(ctx context.Context, in *MintRequest, opts ...grpc.CallOption) (*ResponseTx, error) {
	out := new(ResponseTx)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/mint", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) Send(ctx context.Context, in *SendRequest, opts ...grpc.CallOption) (*ResponseTx, error) {
	out := new(ResponseTx)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/send", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) NewAccount(ctx context.Context, in *NewAccRequest, opts ...grpc.CallOption) (*ResponseTx, error) {
	out := new(ResponseTx)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/newAccount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) Balance(ctx context.Context, in *BalanceRequest, opts ...grpc.CallOption) (*BalanceResponse, error) {
	out := new(BalanceResponse)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/balance", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) SetMaxMoney(ctx context.Context, in *SetMaxMoneyRequest, opts ...grpc.CallOption) (*ResponseTx, error) {
	out := new(ResponseTx)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/setMaxMoney", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) GetMaxMoney(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*BalanceResponse, error) {
	out := new(BalanceResponse)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/getMaxMoney", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) SetStatusProcessing(ctx context.Context, in *StatusProcessing, opts ...grpc.CallOption) (*ResponseTx, error) {
	out := new(ResponseTx)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/setStatusProcessing", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) GetBalancesList(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*ListBalances, error) {
	out := new(ListBalances)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/getBalancesList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) Wallet(ctx context.Context, in *WalletRequest, opts ...grpc.CallOption) (*WalletResponse, error) {
	out := new(WalletResponse)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/wallet", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) GetWalletList(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*ListWalletOwner, error) {
	out := new(ListWalletOwner)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/getWalletList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *walletServiceClient) GetFields(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*FieldsResponse, error) {
	out := new(FieldsResponse)
	err := c.cc.Invoke(ctx, "/proto_buf.WalletService/getFields", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// WalletServiceServer is the server API for WalletService service.
// All implementations should embed UnimplementedWalletServiceServer
// for forward compatibility
type WalletServiceServer interface {
	Mint(context.Context, *MintRequest) (*ResponseTx, error)
	Send(context.Context, *SendRequest) (*ResponseTx, error)
	NewAccount(context.Context, *NewAccRequest) (*ResponseTx, error)
	Balance(context.Context, *BalanceRequest) (*BalanceResponse, error)
	SetMaxMoney(context.Context, *SetMaxMoneyRequest) (*ResponseTx, error)
	GetMaxMoney(context.Context, *empty.Empty) (*BalanceResponse, error)
	SetStatusProcessing(context.Context, *StatusProcessing) (*ResponseTx, error)
	GetBalancesList(context.Context, *empty.Empty) (*ListBalances, error)
	Wallet(context.Context, *WalletRequest) (*WalletResponse, error)
	GetWalletList(context.Context, *empty.Empty) (*ListWalletOwner, error)
	GetFields(context.Context, *empty.Empty) (*FieldsResponse, error)
}

// UnimplementedWalletServiceServer should be embedded to have forward compatible implementations.
type UnimplementedWalletServiceServer struct {
}

func (UnimplementedWalletServiceServer) Mint(context.Context, *MintRequest) (*ResponseTx, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Mint not implemented")
}
func (UnimplementedWalletServiceServer) Send(context.Context, *SendRequest) (*ResponseTx, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Send not implemented")
}
func (UnimplementedWalletServiceServer) NewAccount(context.Context, *NewAccRequest) (*ResponseTx, error) {
	return nil, status.Errorf(codes.Unimplemented, "method NewAccount not implemented")
}
func (UnimplementedWalletServiceServer) Balance(context.Context, *BalanceRequest) (*BalanceResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Balance not implemented")
}
func (UnimplementedWalletServiceServer) SetMaxMoney(context.Context, *SetMaxMoneyRequest) (*ResponseTx, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetMaxMoney not implemented")
}
func (UnimplementedWalletServiceServer) GetMaxMoney(context.Context, *empty.Empty) (*BalanceResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetMaxMoney not implemented")
}
func (UnimplementedWalletServiceServer) SetStatusProcessing(context.Context, *StatusProcessing) (*ResponseTx, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetStatusProcessing not implemented")
}
func (UnimplementedWalletServiceServer) GetBalancesList(context.Context, *empty.Empty) (*ListBalances, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetBalancesList not implemented")
}
func (UnimplementedWalletServiceServer) Wallet(context.Context, *WalletRequest) (*WalletResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Wallet not implemented")
}
func (UnimplementedWalletServiceServer) GetWalletList(context.Context, *empty.Empty) (*ListWalletOwner, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetWalletList not implemented")
}
func (UnimplementedWalletServiceServer) GetFields(context.Context, *empty.Empty) (*FieldsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetFields not implemented")
}

// UnsafeWalletServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to WalletServiceServer will
// result in compilation errors.
type UnsafeWalletServiceServer interface {
	mustEmbedUnimplementedWalletServiceServer()
}

func RegisterWalletServiceServer(s grpc.ServiceRegistrar, srv WalletServiceServer) {
	s.RegisterService(&WalletService_ServiceDesc, srv)
}

func _WalletService_Mint_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(MintRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).Mint(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/mint",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).Mint(ctx, req.(*MintRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_Send_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SendRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).Send(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/send",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).Send(ctx, req.(*SendRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_NewAccount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewAccRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).NewAccount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/newAccount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).NewAccount(ctx, req.(*NewAccRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_Balance_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BalanceRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).Balance(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/balance",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).Balance(ctx, req.(*BalanceRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_SetMaxMoney_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SetMaxMoneyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).SetMaxMoney(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/setMaxMoney",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).SetMaxMoney(ctx, req.(*SetMaxMoneyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_GetMaxMoney_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).GetMaxMoney(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/getMaxMoney",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).GetMaxMoney(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_SetStatusProcessing_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StatusProcessing)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).SetStatusProcessing(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/setStatusProcessing",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).SetStatusProcessing(ctx, req.(*StatusProcessing))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_GetBalancesList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).GetBalancesList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/getBalancesList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).GetBalancesList(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_Wallet_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WalletRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).Wallet(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/wallet",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).Wallet(ctx, req.(*WalletRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_GetWalletList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).GetWalletList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/getWalletList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).GetWalletList(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _WalletService_GetFields_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WalletServiceServer).GetFields(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto_buf.WalletService/getFields",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WalletServiceServer).GetFields(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

// WalletService_ServiceDesc is the grpc.ServiceDesc for WalletService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var WalletService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "proto_buf.WalletService",
	HandlerType: (*WalletServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "mint",
			Handler:    _WalletService_Mint_Handler,
		},
		{
			MethodName: "send",
			Handler:    _WalletService_Send_Handler,
		},
		{
			MethodName: "newAccount",
			Handler:    _WalletService_NewAccount_Handler,
		},
		{
			MethodName: "balance",
			Handler:    _WalletService_Balance_Handler,
		},
		{
			MethodName: "setMaxMoney",
			Handler:    _WalletService_SetMaxMoney_Handler,
		},
		{
			MethodName: "getMaxMoney",
			Handler:    _WalletService_GetMaxMoney_Handler,
		},
		{
			MethodName: "setStatusProcessing",
			Handler:    _WalletService_SetStatusProcessing_Handler,
		},
		{
			MethodName: "getBalancesList",
			Handler:    _WalletService_GetBalancesList_Handler,
		},
		{
			MethodName: "wallet",
			Handler:    _WalletService_Wallet_Handler,
		},
		{
			MethodName: "getWalletList",
			Handler:    _WalletService_GetWalletList_Handler,
		},
		{
			MethodName: "getFields",
			Handler:    _WalletService_GetFields_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "coin.proto",
}
