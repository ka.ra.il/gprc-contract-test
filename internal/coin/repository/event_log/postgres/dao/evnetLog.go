package dao

import (
	"github.com/jackc/pgtype"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	"time"
)

type EventLog struct {
	Address     string       `db:"address"`
	Topics      []string     `db:"topics"`
	Data        []byte       `db:"data"`
	BlockNumber uint64       `db:"blocknumber"`
	TxHash      string       `db:"txhash"`
	TxIndex     uint64       `db:"txindex"`
	BlockHash   string       `db:"blockhash"`
	Index       uint64       `db:"index"`
	Removed     bool         `db:"removed"`
	CreatedAt   time.Time    `db:"createdat"`
	ModifiedAt  time.Time    `db:"modifiedat"`
	EventType   string       `db:"event_type"`
	EventData   pgtype.JSONB `db:"event_data"`
}

func (e EventLog) ToDomainEventLog() (*event_log.EventLog, error) {
	return event_log.New(
		e.Address,
		e.Topics,
		e.Data,
		e.BlockNumber,
		e.TxHash,
		e.TxIndex,
		e.BlockHash,
		e.Index,
		e.Removed,
		e.CreatedAt,
		e.ModifiedAt,
		e.EventType,
		e.EventData,
	)
}

var CreateColumnEventLog = []string{
	"address",
	"topics",
	"data",
	"blocknumber",
	"txhash",
	"txindex",
	"blockhash",
	"index",
	"removed",
	"createdat",
	"modifiedat",
	"event_type",
	"event_data",
}
