include app.env
export

.PHONY: proto
proto:
	@./scripts/proto.sh coin
	@./scripts/proto.sh envelope

.PHONY: abigen
abigen:
	@./scripts/abigen.sh Coin

.PHONY: mockgen
mockgen:
	@./scripts/mockgen.sh

.PHONY: test
test:
	@./scripts/test.sh "coin_event_log_test.go  coin_grpc_test.go  coin_mock_grpc_test.go  coin_simulator_test.go"

.PHONY: deploy
deploy:
	@./scripts/deploy.sh

.PHONY: dock_up
dock_up:
	@./scripts/docker_compose.sh up

.PHONY: dock_down
dock_down:
	@./scripts/docker_compose.sh down

.PHONY: server_run
server_run:
	@./scripts/server.sh

.PHONY: mock
mock:
	@./scripts/mock.sh