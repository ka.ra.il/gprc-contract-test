package coin

import (
	"context"
	"fmt"
	"github.com/jackc/pgtype"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	event_log2 "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/event_log"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	mockStorage "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/storage/mock"
)

var (
	storageRepository = new(mockStorage.EventLog)
	ucDialog          *event_log2.UseCase
	data              = make(map[string]*event_log.EventLog)
	createEventLogs   []*event_log.EventLog
)

func TestMain(m *testing.M) {
	timeNow := time.Now().UTC()
	jsonb := pgtype.JSONB{}
	jsonb.UnmarshalJSON([]byte("{\"to\": \"test-3\", \"amount\": 111}"))
	createEventLog, _ := event_log.New(
		"0x2B2fB79590A2DB5dFb9611287aEe5415d2cFAae5",
		[]string{"", "0x0a4e1e5035320483d096cf114513007c4eda3853a4dca806da2d9faec60470a6"},
		[]byte{},
		563,
		"0x7de44905294c7c6f902a3e70d81645e6cac57b35499b9f7d7d9f48eb3ce5b34b",
		0,
		"0x89dfd00de37ee1a2f55d6dbcee62f0fcfffe71f7fedc4441eedb7e99870d24f9",
		0,
		false,
		timeNow,
		timeNow,
		"Mint",
		jsonb,
	)
	createEventLogs = append(createEventLogs, createEventLog)
	os.Exit(m.Run())
}

func initTestUseCaseEventLog(t *testing.T) {
	assertion := assert.New(t)
	storageRepository.On("CreateEventLog",
		mock.Anything,
		mock.Anything).
		Return(func(ctx context.Context, eventLogs ...*event_log.EventLog) []*event_log.EventLog {
			assertion.Equal(eventLogs, createEventLogs)
			for _, c := range eventLogs {
				data[c.TxHash()] = c
			}
			return eventLogs
		}, func(ctx context.Context, eventLogs ...*event_log.EventLog) error {
			return nil
		})

	storageRepository.On("ReadEventLogByTxHash",
		mock.Anything,
		mock.AnythingOfType("string")).
		Return(func(ctx context.Context, TxHash string) *event_log.EventLog {
			if c, ok := data[TxHash]; ok {
				return c
			}
			return nil
		}, func(ctx context.Context, TxHash string) error {
			if _, ok := data[TxHash]; !ok {
				return fmt.Errorf("not found map by key %s", TxHash)
			}
			return nil
		})

	storageRepository.On("UpdateEventLog",
		mock.Anything,
		mock.Anything).
		Return(func(ctx context.Context, TxHash string, updateFn func(c *event_log.EventLog) (*event_log.EventLog, error)) *event_log.EventLog {
			return nil
		}, func(ctx context.Context, TxHash string, updateFn func(c *event_log.EventLog) (*event_log.EventLog, error)) error {
			return nil
		})
}

func TestEventLog(t *testing.T) {

	initTestUseCaseEventLog(t)
	ucDialog = event_log2.New(storageRepository, event_log2.Options{})

	assertion := assert.New(t)
	t.Run("create eventLog", func(t *testing.T) {
		var ctx = context.Background()

		result, err := ucDialog.Create(ctx, createEventLogs...)
		assertion.NoError(err)
		assertion.Equal(result, createEventLogs)
	})

	t.Run("get eventLog", func(t *testing.T) {
		var ctx = context.Background()

		result, err := ucDialog.ReadByTxHash(ctx, createEventLogs[0].TxHash())
		assertion.NoError(err)
		assertion.Equal(result, createEventLogs[0])
	})
}
