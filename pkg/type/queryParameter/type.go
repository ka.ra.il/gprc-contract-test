package queryParameter

import (
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/pagination"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/sort"
)

type QueryParameter struct {
	Sorts      sort.Sorts
	Pagination pagination.Pagination
}
