package event

import (
	"github.com/ethereum/go-ethereum/crypto"
	"math/big"
)

type LogSend struct {
	From   string   `json:"from"`
	To     string   `json:"to"`
	Amount *big.Int `json:"amount"`
}

type Mint struct {
	To     string   `json:"to"`
	Amount *big.Int `json:"amount"`
}

type NewMaxMoney struct {
	Amount *big.Int `json:"maxMoney"`
}

type NewAccount struct {
	To     string   `json:"to"`
	Amount *big.Int `json:"amount"`
}

type ChangeFlagProcessing struct {
	FlagStatus bool `json:"flagStatus"`
}

type DefaultJson struct {
	FlagStatus bool `json:"event_type_is_undefined"`
}

type LogEventTypeHash struct {
	Sent                 string
	Mint                 string
	NewMaxMoney          string
	NewAccount           string
	ChangeFlagProcessing string
}

func New() LogEventTypeHash {
	return LogEventTypeHash{
		Sent:                 getEventHastStr([]byte("Sent(string,string,uint256)")),
		Mint:                 getEventHastStr([]byte("Mint(string,uint256)")),
		NewMaxMoney:          getEventHastStr([]byte("NewMaxMoney(uint256)")),
		NewAccount:           getEventHastStr([]byte("NewAccount(string,uint256)")),
		ChangeFlagProcessing: getEventHastStr([]byte("ChangeFlagProcessing(bool)")),
	}
}

func getEventHastStr(sig []byte) string {
	return crypto.Keccak256Hash(sig).String()
}
