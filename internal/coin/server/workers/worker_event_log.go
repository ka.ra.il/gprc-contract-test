package workers

import (
	"context"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	producer2 "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event/kafka/producer"
	useCaseEventLog "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/event_log"
	"math/big"
	"strings"
	"time"
)

type EventLogWorker struct {
	Ctx            context.Context
	Log            *logrus.Entry
	UseCase        *useCaseEventLog.UseCase
	Url            string
	Addr           common.Address
	Interval       time.Duration
	ConfigProducer producer2.ConfigKafka
}

func WorkerEventLog(e *EventLogWorker) {
	log := e.Log.WithField("func", "WorkerEventLog")
	client, err := ethclient.Dial(e.Url)
	if err != nil {
		log.Errorf("cannot connect")
	}
	ticker := time.NewTicker(e.Interval)
	defer ticker.Stop()

	contractAbi, err := abi.JSON(strings.NewReader(string(coin.StorageABI)))
	if err != nil {
		log.Errorf("contractAbi reader %s", err)
		return
	}
	eventLogHash := event.New()

	producer, err := producer2.New(e.ConfigProducer)
	if err != nil {
		log.Errorf("kafka producer is not start: %s", err)
	}
	defer producer.Publisher.Close()

	for {
		select {
		case <-ticker.C:
			/** @todo добавить кэш */
			blockNumber, err := e.UseCase.GetLastBlockNumber(context.Background(), e.Addr.String())
			if err != nil {
				log.Errorf("GetLastBlockNumber %s", err)
				return
			}
			sub, err := LogEvent(client, e.Addr, blockNumber+1)
			if err != nil {
				log.Errorf("LogEvent %s", err)
				return
			}
			events, err := event_log.ConvertEthLogToEventLog(&contractAbi, &eventLogHash, sub, log)
			if err != nil {
				log.Errorf("ConvertEthLogToEventLog %s", err)
				return
			}
			if len(events) > 0 {
				//send event log to kafka
				go producer.BatchSend(events)
				_, err = e.UseCase.Create(context.Background(), events...)
				if err != nil {
					log.Errorf("useCase.Create %s", err)
					return
				}
				log.Infof("%d event(s) saved", len(events))
			}

		case <-e.Ctx.Done():
			log.Info("exit")
			return
		}
	}
}

func LogEvent(client *ethclient.Client, contractAddress common.Address, blockNumber int64) ([]types.Log, error) {
	//header, err := client.HeaderByNumber(context.Background(), nil)
	//if err != nil {
	//	return nil, err
	//}

	query := ethereum.FilterQuery{
		Addresses: []common.Address{contractAddress},
		FromBlock: big.NewInt(blockNumber),
	}
	sub, err := client.FilterLogs(context.Background(), query)
	if err != nil {
		return nil, err
	}
	return sub, nil
}
