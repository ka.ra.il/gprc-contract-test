package consumer

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/hamba/avro"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event/kafka/dao"
)

const UUIDHeaderKey = "_avro_message_uuid"

type AvroMarshaller struct {
	schemaRegisterUrl string //http://127.0.0.1:8081
	subjectName       string
}

func NewAvroMarshaller(schemaRegisterUrl string, subjectName string) *AvroMarshaller {
	return &AvroMarshaller{schemaRegisterUrl: schemaRegisterUrl, subjectName: subjectName}
}

func (am AvroMarshaller) Unmarshal(kafkaMsg *sarama.ConsumerMessage) (*message.Message, error) {
	//reg, err := registry.NewClient(am.schemaRegisterUrl)
	//if err != nil {
	//	return nil, err
	//}

	//schemas, err := reg.GetSubjects()
	//if err != nil {
	//	return nil, err
	//}
	//
	//fmt.Println("schemas: ", schemas)

	//schema, err := reg.GetLatestSchema(am.subjectName)
	//if err != nil {
	//	return nil, err
	//}
	//fmt.Println(schema.String())

	schemaStrr := `{
			   "type":"record",
			   "name":"Value",
			   "fields":[
				  {
					 "name":"address",
					 "type": "string"
				  }
			   ],
			   "connect.name":"db_.public.event_log.Value"
     }`

	//fmt.Println(reg.IsRegistered(am.subjectName, schema.String()))

	schema, err := avro.Parse(schemaStrr)
	fmt.Println("schema: ", schema)

	out := dao.EventLog{}
	err = avro.Unmarshal(schema, kafkaMsg.Value, &out)
	if err != nil {
		return nil, err
	}
	fmt.Println("out", out)

	var messageID string
	metadata := make(message.Metadata, len(kafkaMsg.Headers))

	for _, header := range kafkaMsg.Headers {
		if string(header.Key) == UUIDHeaderKey {
			messageID = string(header.Value)
		} else {
			metadata.Set(string(header.Key), string(header.Value))
		}
	}

	msg := message.NewMessage(messageID, kafkaMsg.Value)
	msg.Metadata = metadata

	return msg, nil
}
