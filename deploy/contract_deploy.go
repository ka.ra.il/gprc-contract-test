package main

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"github.com/sirupsen/logrus"
	config2 "gitlab.com/ka.ra.il/grpc-contract-test/config"
	store "gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/eth"
	"math/big"
	"os/exec"
	"time"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

func main() {

	c := config2.Config{}
	err := config2.NewConfig(&c, "./../")
	if err != nil {
		logrus.Fatalf("ошибка конфигурации: %s", err)
	}
	client, err := ethclient.Dial(c.Url)
	if err != nil {
		logrus.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA(c.AccountPrivateKey[2:])
	if err != nil {
		logrus.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		logrus.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		logrus.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		logrus.Fatal(err)
	}

	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(int64(c.ChainId)))
	if err != nil {
		logrus.Fatal(err)
	}
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	//auth.GasLimit = uint64(25000000)
	auth.GasPrice = gasPrice

	address, tx, _, err := store.DeployStorage(auth, client)
	if err != nil {
		logrus.Fatal(err)
	}

	eth.WaitTxConfirmed(eth.WaitTx{
		Ctx:       context.Background(),
		Client:    client,
		Hash:      tx.Hash(),
		DurTicker: 10 * time.Microsecond,
		DurTimer:  3 * time.Second,
	})

	receipt, err := client.TransactionReceipt(context.Background(), tx.Hash())
	if err != nil {
		logrus.Fatal(err)
	}
	if receipt.Status != 1 {
		logrus.Fatalf("receipt status is 0 tx: %s", tx.Hash().String())
	}
	logrus.Infof("new contract adderss %s nonce %d", address.Hex(), nonce)

	findEnv := "QUORUM_CONTRACT_WALLET_ADDRESS="
	_, err = replaceFileRow(findEnv, fmt.Sprintf("%s%s", findEnv, address.Hex()), "../app.env")
	if err != nil {
		logrus.Fatal(err)
	}
	findEnvJson := "\"QUORUM_CONTRACT_WALLET_ADDRESS\""
	_, err = replaceFileRow(findEnvJson, fmt.Sprintf("%s:\"%s\",", findEnvJson, address.Hex()), "../app.json")
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Infof("value env variable %s replaced to %s in env files", findEnv[:len(findEnv)-1], address.Hex())
}

func replaceFileRow(findSymbols, replaceSymbols, pathFile string) (string, error) {
	strReplace := fmt.Sprintf(
		"s/%s.*/%s/g", findSymbols, replaceSymbols)
	cmd := exec.Command("sed", "-i", strReplace, pathFile)
	out, err := cmd.Output()
	if err != nil {
		return "", err
	}
	return string(out), nil
}
