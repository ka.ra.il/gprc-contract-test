package grpc

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	store "gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/eth"
	"math/big"
	"time"
)

type GrpcServer struct {
	StoreSession *store.StorageSession
	Client       *ethclient.Client
}

func NewGrpcServer(url string, storeSession *store.StorageSession) (GrpcServer, error) {
	client, err := ethclient.Dial(url)
	if err != nil {
		return GrpcServer{}, err
	}
	return GrpcServer{StoreSession: storeSession, Client: client}, nil
}

func (g GrpcServer) Mint(ctx context.Context, request *coin.MintRequest) (*coin.ResponseTx, error) {
	tx, err := g.StoreSession.Mint(request.Address, big.NewInt(int64(request.Amount)))
	if err != nil {
		return nil, err
	}

	eth.WaitTxConfirmed(eth.WaitTx{
		Ctx:       context.Background(),
		Client:    g.Client,
		Hash:      tx.Hash(),
		DurTicker: 10 * time.Microsecond,
		DurTimer:  3 * time.Second,
	})

	receipt, err := g.Client.TransactionReceipt(context.Background(), tx.Hash())
	if err != nil {
		return nil, err
	}
	if receipt.Status != 1 {
		errors.Errorf("receipt status is 0 tx: %s", tx.Hash().String())
	}
	return &coin.ResponseTx{Msg: "баланс адреса владельца успешно обновлен", TxHash: tx.Hash().String()}, nil
}

func (g GrpcServer) Send(ctx context.Context, request *coin.SendRequest) (*coin.ResponseTx, error) {
	tx, err := g.StoreSession.Send(request.From, request.To, big.NewInt(int64(request.Amount)))
	if err != nil {
		return nil, err
	}
	return &coin.ResponseTx{Msg: fmt.Sprintf("баланс адреса %s увеличен на %d", request.To, request.Amount), TxHash: tx.Hash().String()}, nil
}

func (g GrpcServer) NewAccount(ctx context.Context, request *coin.NewAccRequest) (*coin.ResponseTx, error) {
	fmt.Println(request.Address, big.NewInt(int64(request.Amount)))
	tx, err := g.StoreSession.NewAccount(request.Address, big.NewInt(int64(request.Amount)))
	if err != nil {
		return nil, err
	}
	return &coin.ResponseTx{Msg: fmt.Sprintf("кошелек с адресом %s успешно добален", request.Address), TxHash: tx.Hash().String()}, nil
}

func (g GrpcServer) Balance(ctx context.Context, request *coin.BalanceRequest) (*coin.BalanceResponse, error) {
	amount, err := g.StoreSession.Wallets(request.Address)
	if err != nil {
		return nil, err
	}
	return &coin.BalanceResponse{Amount: amount.Val.Uint64()}, nil
}

func (g GrpcServer) SetMaxMoney(ctx context.Context, request *coin.SetMaxMoneyRequest) (*coin.ResponseTx, error) {
	oldAmount, err := g.StoreSession.GetMaxMoney()
	if err != nil {
		return nil, err
	}
	tx, err := g.StoreSession.SetMaxMoney(big.NewInt(int64(request.Amount)))
	if err != nil {
		return nil, err
	}

	return &coin.ResponseTx{TxHash: tx.Hash().String(), Msg: fmt.Sprintf("успешно изменено с %d на %d", oldAmount, request.Amount)}, nil
}

func (g GrpcServer) GetMaxMoney(ctx context.Context, empty *empty.Empty) (*coin.BalanceResponse, error) {
	amount, err := g.StoreSession.GetMaxMoney()
	if err != nil {
		return nil, err
	}
	return &coin.BalanceResponse{Amount: amount.Uint64()}, nil
}

func (g GrpcServer) SetStatusProcessing(ctx context.Context, processing *coin.StatusProcessing) (*coin.ResponseTx, error) {
	tx, err := g.StoreSession.SetStatusProcessing(processing.Status)
	if err != nil {
		return nil, err
	}
	return &coin.ResponseTx{TxHash: tx.Hash().String()}, nil
}

func (g GrpcServer) GetBalancesList(ctx context.Context, empty *empty.Empty) (*coin.ListBalances, error) {
	addresses, values, ids, err := g.StoreSession.GetBalancesList()
	if err != nil {
		return nil, err
	}
	res := make(map[string]*coin.Wallet)
	for k, v := range addresses {
		res[v] = &coin.Wallet{
			Val: values[k].Uint64(),
			Id:  ids[k].Uint64(),
		}
	}
	return &coin.ListBalances{Balances: res}, nil
}

func (g GrpcServer) GetFields(ctx context.Context, e *empty.Empty) (*coin.FieldsResponse, error) {
	maxMoney, isSendProcessing, cntWallets, seed, err := g.StoreSession.GetFields()
	if err != nil {
		return nil, err
	}
	return &coin.FieldsResponse{
		MaxMoney:         maxMoney.Uint64(),
		IsSendProcessing: isSendProcessing,
		CntWallets:       cntWallets.Uint64(),
		Seed:             seed.Uint64(),
	}, nil
}

func (g GrpcServer) Wallet(ctx context.Context, request *coin.WalletRequest) (*coin.WalletResponse, error) {
	address, err := g.StoreSession.WalletOwner(big.NewInt(int64(request.Id)))
	if err != nil {
		return nil, err
	}
	return &coin.WalletResponse{Address: address}, nil
}

func (g GrpcServer) GetWalletList(ctx context.Context, e *empty.Empty) (*coin.ListWalletOwner, error) {
	addresses, _, ids, err := g.StoreSession.GetBalancesList()
	if err != nil {
		return nil, err
	}
	res := make(map[uint64]string)
	for k, v := range addresses {
		res[ids[k].Uint64()] = v
	}
	return &coin.ListWalletOwner{WalletOwner: res}, nil
}

func waitForReceipt(c *ethclient.Client, hx common.Hash) (*types.Receipt, error) {
	return nil, nil
}
