package event_log

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
	"time"
)

func (uc *UseCase) Create(ctx context.Context, events ...*event_log.EventLog) ([]*event_log.EventLog, error) {
	return uc.adapterStorage.CreateEventLog(ctx, events...)
}

func (uc *UseCase) Update(ctx context.Context, eventlogUpdate event_log.EventLog) (*event_log.EventLog, error) {
	return uc.adapterStorage.UpdateEventLog(ctx, eventlogUpdate.TxHash(), func(oldEventLog *event_log.EventLog) (*event_log.EventLog, error) {
		timeNow := time.Now().UTC()
		return event_log.New(
			eventlogUpdate.Address(),
			eventlogUpdate.Topics(),
			[]byte(eventlogUpdate.Data()),
			eventlogUpdate.BlockNumber(),
			eventlogUpdate.TxHash(),
			eventlogUpdate.TxIndex(),
			eventlogUpdate.BlockHash(),
			eventlogUpdate.Index(),
			eventlogUpdate.Removed(),
			timeNow,
			timeNow,
			eventlogUpdate.EventType(),
			eventlogUpdate.EventData(),
		)
	})
}

func (uc *UseCase) Delete(ctx context.Context, TxHash string) error {
	return uc.adapterStorage.DeleteEventLog(ctx, TxHash)
}

func (uc *UseCase) List(c context.Context, parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error) {

	span, ctx := opentracing.StartSpanFromContext(c, "List")
	defer span.Finish()

	return uc.adapterStorage.ListEventLog(ctx, parameter)
}

func (uc *UseCase) ReadByTxHash(ctx context.Context, TxHash string) (response *event_log.EventLog, err error) {

	return uc.adapterStorage.ReadEventLogByTxHash(ctx, TxHash)
}

func (uc *UseCase) Count(c context.Context) (uint64, error) {
	span, ctx := opentracing.StartSpanFromContext(c, "Count")
	defer span.Finish()

	return uc.adapterStorage.CountEventLog(ctx)
}

func (uc *UseCase) GetLastBlockNumber(ctx context.Context, addressContract string) (int64, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "GetLastBlockNumber")
	defer span.Finish()
	return uc.adapterStorage.GetLastBlockNumber(ctx, addressContract)
}
