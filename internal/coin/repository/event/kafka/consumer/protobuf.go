package consumer

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/pkg/errors"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event/kafka/protobuf_lib"
	event_log "gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/proto_buf"
)

type ProtobufMarshaller struct {
	Deserialize *protobuf_lib.ProtobufDeserializer
	Topic       string
}

func NewProtobufMarshaller(deserialize protobuf_lib.ProtobufDeserializer, topic string) *ProtobufMarshaller {
	return &ProtobufMarshaller{Deserialize: &deserialize, Topic: topic}
}

func (m ProtobufMarshaller) Marshal(topic string, msg *message.Message) (*sarama.ProducerMessage, error) {
	return nil, nil
}

func (m ProtobufMarshaller) Unmarshal(kafkaMsg *sarama.ConsumerMessage) (*message.Message, error) {
	marshal := kafka.DefaultMarshaler{}
	msg, err := marshal.Unmarshal(kafkaMsg)
	if err != nil {
		return nil, err
	}

	value, err := m.Deserialize.Deserialize(&m.Topic, msg.Payload)
	if err != nil {
		return nil, fmt.Errorf("Error consuming the message: %v (%v)", err, value)
	}
	fmt.Errorf("value ---", value)

	switch v := value.(type) {
	case *event_log.Envelope:
		if kafkaMsg.Headers != nil {
			fmt.Printf("%% Headers: %v\n envelope.Envelope", kafkaMsg.Headers)
		}
		return msg, nil
	default:
		fmt.Println("v type", v)
		return nil, errors.New("not switched")
	}
	return nil, errors.New("not switched the end")

}
