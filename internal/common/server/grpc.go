package server

import (
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/log"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/common/logs"
	"google.golang.org/grpc"
	"net"
	"os"
)

func init() {
	logger := logrus.New()
	logs.SetFormatter(logger)
	logger.SetLevel(logrus.WarnLevel)

	grpc_logrus.ReplaceGrpcLogger(logrus.NewEntry(logger))
}

func RunGRPCServer(ctx context.Context, registerServer func(server *grpc.Server)) {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	addr := fmt.Sprintf(":%s", port)
	RunGRPCServerOnAddr(ctx, addr, registerServer)
}

func RunGRPCServerOnAddr(ctx context.Context, addr string, registerServer func(server *grpc.Server)) {
	logrusEntry := logrus.NewEntry(logrus.StandardLogger())

	grpcServer := grpc.NewServer(
		grpc_middleware.WithUnaryServerChain(
			grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.UnaryServerInterceptor(logrusEntry),
		),
		grpc_middleware.WithStreamServerChain(
			grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.StreamServerInterceptor(logrusEntry),
		),
	)
	registerServer(grpcServer)

	listen, err := net.Listen("tcp", addr)
	if err != nil {
		logrus.Fatalf("net listen address %s: %s", addr, err)
	}
	go func(ctx context.Context, server *grpc.Server) {
		<-ctx.Done()
		log.Info("grpc GracefulStop")
		server.GracefulStop()
	}(ctx, grpcServer)
	logrus.WithField("grpcEndpoint", addr).Info("Starting: gRPC Listener")
	err = grpcServer.Serve(listen)
	if err != nil {
		logrus.Fatalf("grpc server: %s", err)
	}
}
