package dao

import (
	"github.com/jackc/pgtype"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	"time"
)

type KafkaSaveTopic struct {
	topic     string       `db:"topic"`
	eventData pgtype.JSONB `db:"data"`
	createdAt time.Time    `db:"createdat"`
}

func (k KafkaSaveTopic) ToDomainEventLog() (*kafka_save_topic.KafkaSaveTopic, error) {
	return kafka_save_topic.New(
		k.topic,
		k.eventData,
		k.createdAt,
	)
}

var CreateColumnEventLog = []string{
	"topic",
	"data",
	"createdat",
}
