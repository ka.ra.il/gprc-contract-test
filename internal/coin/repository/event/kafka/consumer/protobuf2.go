package consumer

import (
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/confluentinc/confluent-kafka-go/v2/schemaregistry"
	"github.com/confluentinc/confluent-kafka-go/v2/schemaregistry/serde"
	"github.com/confluentinc/confluent-kafka-go/v2/schemaregistry/serde/protobuf"
	"github.com/mitchellh/mapstructure"
	"github.com/riferrei/srclient"
	event_log "gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/proto_buf"
	"io/ioutil"
	"os"
)

type ProtobufMarshaller2 struct {
	SchemaRegistryClient *srclient.SchemaRegistryClient
	Deser                *protobuf.Deserializer
	Topic                string
}

func NewProtobufMarshaller2(url, topic string) *ProtobufMarshaller2 {
	client, err := schemaregistry.NewClient(schemaregistry.NewConfig(url))
	deser, err := protobuf.NewDeserializer(client, serde.ValueSerde, protobuf.NewDeserializerConfig())

	if err != nil {
		fmt.Printf("Failed to create deserializer: %s\n", err)
	}

	SrClient := srclient.CreateSchemaRegistryClient(url)
	pwd, _ := os.Getwd()
	schemaBytes, err := ioutil.ReadFile(pwd + "/../../../proto_buf/envelope.proto")
	if err != nil {
		fmt.Printf("can not schemaBytes read file: %s\n", err)
	}
	schema, err := SrClient.CreateSchema("dbprotobuf_event_log-value", string(schemaBytes), srclient.Protobuf)
	fmt.Println("schema, err", schema, err)
	deser.ProtoRegistry.RegisterMessage((&event_log.Envelope{}).ProtoReflect().Type())
	return &ProtobufMarshaller2{
		SchemaRegistryClient: SrClient,
		Deser:                deser,
		Topic:                topic,
	}
}

func (m ProtobufMarshaller2) Marshal(topic string, msg *message.Message) (*sarama.ProducerMessage, error) {
	return nil, nil
}

func (m ProtobufMarshaller2) Unmarshal(kafkaMsg *sarama.ConsumerMessage) (*message.Message, error) {
	marshal := kafka.DefaultMarshaler{}
	msg, err := marshal.Unmarshal(kafkaMsg)
	if err != nil {
		return nil, err
	}
	//schemaID := binary.BigEndian.Uint32(msg.Payload[1:5])
	//
	//schema, err := m.SchemaRegistryClient.GetSchema(int(schemaID))
	//if err != nil {
	//	return nil, err
	//}
	kafkaMsg.Value[4] = 7
	value, err := m.Deser.Deserialize(m.Topic, kafkaMsg.Value)
	fmt.Println("value, err", value, err)
	data := event_log.Envelope{}
	mapstructure.Decode(value, &data)
	fmt.Println(data.After.Topics)
	return msg, nil

}
