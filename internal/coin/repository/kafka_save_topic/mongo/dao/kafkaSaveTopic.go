package dao

import (
	"github.com/jackc/pgtype"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

type KafkaSaveTopic struct {
	topic     string                 `bson:"topic"`
	eventData map[string]interface{} `bson:"data"`
	createdAt time.Time              `bson:"createdat"`
}

func New(topic string, eventData map[string]interface{}, createdAt time.Time) (KafkaSaveTopic, error) {
	return KafkaSaveTopic{topic: topic, eventData: eventData, createdAt: createdAt}, nil
}

func (k KafkaSaveTopic) ToDomainEventLog() (*kafka_save_topic.KafkaSaveTopic, error) {
	var jsonb pgtype.JSONB
	err := jsonb.Scan(k.eventData)
	if err != nil {
		return nil, err
	}
	return kafka_save_topic.New(
		k.topic,
		jsonb,
		k.createdAt,
	)
}

func (k KafkaSaveTopic) MarshalBSON() ([]byte, error) {
	return bson.Marshal(struct {
		Topic     string                 `bson:"topic"`
		EventData map[string]interface{} `bson:"data"`
		CreatedAt time.Time              `bson:"createdat"`
	}{
		Topic:     k.topic,
		EventData: k.eventData,
		CreatedAt: k.createdAt,
	})
}

var CreateColumnEventLog = []string{
	"topic",
	"data",
	"createdat",
}
