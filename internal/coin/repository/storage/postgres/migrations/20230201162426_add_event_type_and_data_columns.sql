-- +goose Up
-- +goose StatementBegin
ALTER TABLE event_log ADD COLUMN IF NOT EXISTS  event_type VARCHAR(128);
ALTER TABLE event_log ADD COLUMN IF NOT EXISTS  event_data JSONB NOT NULL DEFAULT '{}'::jsonb;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE event_log DROP COLUMN IF EXISTS event_type;;
ALTER TABLE event_log DROP COLUMN IF EXISTS  event_data;;
-- +goose StatementEnd

