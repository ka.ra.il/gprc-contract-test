package postgres

import (
	"context"
	"errors"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	dao "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/event_log/postgres/dao"

	"github.com/Masterminds/squirrel"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"

	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/transaction"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/columnCode"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
)

var mappingSort = map[columnCode.ColumnCode]string{
	"id":          "id",
	"name":        "name",
	"description": "description",
}

func (r *Repository) CreateEventLog(ctx context.Context, eventLog *event_log.EventLog) (*event_log.EventLog, error) {
	query, args, err := r.genSQL.Insert("event_log").
		Columns(
			"address",
			"topics",
			"data",
			"blocknumber",
			"txhash",
			"txindex",
			"blockhash",
			"index",
			"removed",
			"createdat",
			"modifiedat",
		).
		Values(
			eventLog.Address(),
			eventLog.Topics(),
			eventLog.Data(),
			eventLog.BlockNumber(),
			eventLog.TxHash(),
			eventLog.TxIndex(),
			eventLog.BlockHash(),
			eventLog.Index(),
			eventLog.Removed(),
			eventLog.CreatedAt(),
			eventLog.ModifiedAt(),
		).
		ToSql()
	if err != nil {
		return nil, err
	}

	if _, err = r.db.Exec(ctx, query, args...); err != nil {
		return nil, err
	}
	return eventLog, nil
}

func (r *Repository) UpdateEventLog(TxHash string, updateFn func(eventLog *event_log.EventLog) (*event_log.EventLog, error)) (*event_log.EventLog, error) {
	var ctx = context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	upEventLog, err := r.oneEventLogTx(ctx, tx, TxHash)
	if err != nil {
		return nil, err
	}
	eventLogForUpdate, err := updateFn(upEventLog)
	if err != nil {
		return nil, err
	}

	query, args, err := r.genSQL.Update("event_log").
		Set("address", eventLogForUpdate.Address()).
		Set("topics", eventLogForUpdate.Topics()).
		Set("data", eventLogForUpdate.TxHash()).
		Where(squirrel.And{
			squirrel.Eq{
				"txhash": TxHash,
			},
		}).
		Suffix(`RETURNING
			id,
			name,
			description,
			created_at,
			modified_at`,
		).
		ToSql()
	if err != nil {
		return nil, err
	}

	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var daoEventLog []*dao.EventLog
	if err = pgxscan.ScanAll(&daoEventLog, rows); err != nil {
		return nil, err
	}

	return eventLogForUpdate, nil
}

func (r *Repository) DeleteEventLog(TxHash string) error {
	var ctx = context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	if err = r.deleteEventLogTx(ctx, tx, TxHash); err != nil {
		return err
	}

	return nil
}

func (r *Repository) deleteEventLogTx(ctx context.Context, tx pgx.Tx, TxHash string) error {
	query, args, err := r.genSQL.
		Delete("event_log").
		Where(squirrel.Eq{"id": TxHash}).
		ToSql()

	if err != nil {
		return err
	}

	if _, errEx := tx.Exec(ctx, query, args...); errEx != nil {
		return err
	}

	return nil
}

func (r *Repository) ListEventLog(parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error) {
	var ctx = context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.listEventLogTx(ctx, tx, parameter)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) listEventLogTx(ctx context.Context, tx pgx.Tx, parameter queryParameter.QueryParameter) ([]*event_log.EventLog, error) {
	var result []*event_log.EventLog

	var builder = r.genSQL.Select(
		"address",
		"topics",
		"data",
		"blocknumber",
		"txhash",
		"txindex",
		"blockhash",
		"index",
		"removed",
		"createdat",
		"modifiedat",
	).
		From("event_log")

	if len(parameter.Sorts) > 0 {
		builder = builder.OrderBy(parameter.Sorts.Parsing(mappingSort)...)
	} else {
		builder = builder.OrderBy("created_at DESC")
	}

	if parameter.Pagination.Limit > 0 {
		builder = builder.Limit(parameter.Pagination.Limit)
	}
	if parameter.Pagination.Offset > 0 {
		builder = builder.Offset(parameter.Pagination.Offset)
	}

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var events []*dao.EventLog
	if err = pgxscan.ScanAll(&events, rows); err != nil {
		return nil, err
	}

	for _, g := range events {
		domainEventLog, err := g.ToDomainEventLog()
		if err != nil {
			return nil, err
		}
		result = append(result, domainEventLog)
	}
	return result, nil
}

func (r *Repository) ReadGroupByID(TxHash string) (*event_log.EventLog, error) {
	var ctx = context.Background()

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.oneEventLogTx(ctx, tx, TxHash)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) oneEventLogTx(ctx context.Context, tx pgx.Tx, TxHash string) (response *event_log.EventLog, err error) {

	var builder = r.genSQL.Select(
		"address",
		"topics",
		"data",
		"blocknumber",
		"txhash",
		"txindex",
		"blockhash",
		"index",
		"removed",
		"createdat",
		"modifiedat",
	).
		From("event_log").Where(squirrel.Eq{"txhash": tx})

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var daoEventLog []*dao.EventLog
	if err = pgxscan.ScanAll(&daoEventLog, rows); err != nil {
		return nil, err
	}

	if len(daoEventLog) == 0 {
		return nil, errors.New("eventLog not found")
	}
	return daoEventLog[0].ToDomainEventLog()

}

func (r *Repository) CountEventLog() (uint64, error) {
	var ctx = context.Background()

	var builder = r.genSQL.Select(
		"COUNT(id)",
	).From("event_log")

	builder = builder.Where(squirrel.Eq{"is_archived": false})

	query, args, err := builder.ToSql()
	if err != nil {
		return 0, err
	}

	row := r.db.QueryRow(ctx, query, args...)
	var total uint64

	if err = row.Scan(&total); err != nil {
		return 0, err
	}

	return total, nil
}
