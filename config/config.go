package config

import (
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/contract_store"
	"math/big"
)

type Config struct {
	ServiceName               string         `mapstructure:"SERVICE_NAME"`
	Url                       string         `mapstructure:"QUORUM_URL"`
	Ws                        string         `mapstructure:"QUORUM_WS"`
	ChainId                   int            `mapstructure:"CHAIN_ID"`
	AccountAddress            string         `mapstructure:"QUORUM_ADDRESS"`
	AccountPrivateKey         string         `mapstructure:"QUORUM_PRIVATE_KEY"`
	ContractAddress           string         `mapstructure:"QUORUM_CONTRACT_ADDRESS"`
	GrpcSetTransferUrl        string         `mapstructure:"GRPC_SET_TRANSFER_URL"`
	GrpcWalletUrl             string         `mapstructure:"GRPC_WALLET_URL"`
	ContractAddressWallet     string         `mapstructure:"QUORUM_CONTRACT_WALLET_ADDRESS"`
	OwnerAddressWallet        string         `mapstructure:"WALLET_OWNER_ADDRESS"`
	ReceiverAddressesWallet   []string       `mapstructure:"WALLET_RECEIVER_ADDRESSES"`
	PostgresConfig            PostgresConfig `mapstructure:"POSTGRES_CONFIG"`
	JaegerConfig              JaegerConfig   `mapstructure:"JAEGER_CONFIG"`
	KafkaConfig               KafkaConfig    `mapstructure:"KAFKA_CONFIG"`
	WorkerEventLogWaitTimeout int            `mapstructure:"WORKER_EVENT_LOG_WAIT_TIMEOUT"` //second
	MongoConfig               MongoConfig    `mapstructure:"MONGO_CONFIG"`
}

type PostgresConfig struct {
	Host         string `mapstructure:"POSTGRES_HOST"`
	Port         uint16 `mapstructure:"POSTGRES_PORT"`
	Password     string `mapstructure:"POSTGRES_PASSWORD"`
	User         string `mapstructure:"POSTGRES_USER"`
	Name         string `mapstructure:"POSTGRES_DB"`
	MigrationDir string `mapstructure:"POSTGRES_MIGRATIONS_DIR"`
	SSLMode      string `mapstructure:"POSTGRES_SSL_MODE"`
}

type JaegerConfig struct {
	Host string `mapstructure:"JAEGER_AGENT_HOST"`
	Port int    `mapstructure:"JAEGER_AGENT_PORT"`
}

type KafkaConfig struct {
	Brokers        []string `mapstructure:"KAFKA_BROKERS"`
	TopicSuccessTx string   `mapstructure:"KAFKA_TOPIC_SUCCESS_TX"`
	TopicErrorTx   string   `mapstructure:"KAFKA_TOPIC_ERROR_TX"`
}

type MongoConfig struct {
	Host         string `mapstructure:"MONGO_HOST"`
	Port         int    `mapstructure:"MONGO_PORT"`
	DbName       string `mapstructure:"MONGO_DB_NAME"`
	UserName     string `mapstructure:"MONGO_USER_NAME"`
	Password     string `mapstructure:"MONGO_PASSWORD"`
	RootUserName string `mapstructure:"MONGO_ROOT_USERNAME"`
	RootPassword string `mapstructure:"MONGO_ROOT_PASSWORD"`
}

func NewConfig(c *Config, path string) error {
	viper.SetConfigName("app")
	viper.SetConfigType("json")
	viper.AddConfigPath(path)
	err := viper.ReadInConfig()
	if err != nil {
		return fmt.Errorf("fatal error config file: %w", err)
	}

	configOption := viper.DecodeHook(mapstructure.ComposeDecodeHookFunc(
		mapstructure.StringToSliceHookFunc(","),
	))

	err = viper.Unmarshal(c, configOption)
	if err != nil {
		return fmt.Errorf("fatal error unmarshal: %w", err)
	}
	return nil
}

func (c *Config) NewStore() (*contract_store.Store, error) {
	client, err := ethclient.Dial(c.Url)
	if err != nil {
		return nil, err
	}
	address := common.HexToAddress(c.ContractAddressWallet)
	instance, err := coin.NewStorage(address, client)
	if err != nil {
		return nil, err
	}

	privateKey, err := crypto.HexToECDSA(c.AccountPrivateKey[2:])
	if err != nil {
		return nil, err
	}
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, big.NewInt(int64(c.ChainId)))
	if err != nil {
		return nil, err
	}
	return &contract_store.Store{
		ContractAddress: address,
		StoreSession: &coin.StorageSession{
			Contract:     instance,
			TransactOpts: *auth,
		},
		UrlHttp: c.Url,
		UrlWs:   c.Ws,
	}, nil
}
