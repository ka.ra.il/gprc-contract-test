package workers

import (
	"context"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	useCaseEventLog "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/event_log"
	"strings"
)

func SubscribeEvent(ctx context.Context, log *logrus.Entry, useCase *useCaseEventLog.UseCase, url string, contractAddress common.Address) {
	log = log.WithField("func", "SubscribeEvent")
	client, err := ethclient.Dial(url)
	if err != nil {

	}
	query := ethereum.FilterQuery{
		Addresses: []common.Address{contractAddress},
	}

	logs := make(chan types.Log)
	sub, err := client.SubscribeFilterLogs(context.Background(), query, logs)
	if err != nil {
		log.Errorf("can not subcribe event notification %s", err)
		return
	}

	contractAbi, err := abi.JSON(strings.NewReader(string(coin.StorageABI)))
	if err != nil {
		log.Fatal(err)
	}

	eventLogHash := event.New()

	for {
		select {
		case err := <-sub.Err():
			log.Error(err)
		case vLog := <-logs:
			events, err := event_log.ConvertEthLogToEventLog(&contractAbi, &eventLogHash, []types.Log{vLog}, log)
			if err != nil {
				log.Errorf("ConvertEthLogToEventLog %s", err)
			}
			_, err = useCase.Create(context.Background(), events...)
			if err != nil {
				log.Errorf("useCase.Create %s", err)
			}
		case <-ctx.Done():
			log.Info("exit")
			return
		}
	}
}
