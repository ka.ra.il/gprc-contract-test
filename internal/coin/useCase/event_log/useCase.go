package event_log

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/adapters/storage"
)

type UseCase struct {
	adapterStorage storage.EventLog
	options        Options
}

type Options struct{}

func New(storage storage.EventLog, options Options) *UseCase {
	var uc = &UseCase{
		adapterStorage: storage,
	}
	uc.SetOptions(options)
	return uc
}

func (uc *UseCase) SetOptions(options Options) {
	if uc.options != options {
		uc.options = options
		log.Infof("set new options %v", uc.options)
	}
}
