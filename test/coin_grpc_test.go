package coin

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jackc/pgtype"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	repositoryStorage "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/storage/postgres"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/converter"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/store/postgres"
	"math/big"
	"testing"
	"time"

	//"fmt"
	config2 "gitlab.com/ka.ra.il/grpc-contract-test/config"
	"log"
	"net"
	//"testing"
	//"time"

	kitgrpc "github.com/go-kit/kit/transport/grpc"
	"github.com/pkg/errors"
	pb "gitlab.com/ka.ra.il/grpc-contract-test/internal/common/genproto/coin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/test/bufconn"
	//"google.golang.org/protobuf/types/known/timestamppb"
	_ "github.com/golang/mock/mockgen/model"
	grpc2 "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/server/grpc"
)

const (
	addAmountCycle = 10
	mintAddress    = "test_mint"
)

type testAddr struct {
	Address string
	Amount  uint64
}

type EventLogDb struct {
	TxHash    string
	EventType string
	EventData pgtype.JSONB
}

var (
	testAddrs   = make([]testAddr, 0)
	mintAddr    = testAddr{Address: mintAddress}
	EventLogDbs = make([]EventLogDb, 0)
)

func init() {
	for id := 1; id < 5; id++ {
		testAddrs = append(testAddrs, testAddr{Address: fmt.Sprintf("test-%d", id)})
	}
}

func Server(ctx context.Context) (pb.WalletServiceClient, func(), config2.Config) {
	buffer := 1024 * 1024
	listener := bufconn.Listen(buffer)

	baseServer := grpc.NewServer(grpc.UnaryInterceptor(kitgrpc.Interceptor))

	c := config2.Config{}
	err := config2.NewConfig(&c, "./..")
	if err != nil {
		log.Fatalf("ошибка конфигурации: %s", err)
	}

	store, err := c.NewStore()
	if err != nil {
		log.Fatalf("ошибка при создании store: %s", err)
	}

	svc, err := grpc2.NewGrpcServer(store.UrlHttp, store.StoreSession)
	if err != nil {
		log.Fatalf("create NewGrpcServer: %s", err)
	}

	pb.RegisterWalletServiceServer(baseServer, svc)
	go func() {
		if err := baseServer.Serve(listener); err != nil {
			log.Fatalf("baseServer.Serve: %s", err)
		}
	}()

	conn, _ := grpc.DialContext(ctx, "", grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}), grpc.WithTransportCredentials(insecure.NewCredentials()))

	closer := func() {
		listener.Close()
		baseServer.Stop()
	}

	client := pb.NewWalletServiceClient(conn)

	return client, closer, c
}

func ServerDb(ctx context.Context) (*repositoryStorage.Repository, func(), time.Duration, error) {
	c := config2.Config{}
	err := config2.NewConfig(&c, "./..")

	timeWait := time.Duration(c.WorkerEventLogWaitTimeout+5) * time.Second

	log := logrus.NewEntry(logrus.StandardLogger())
	if err != nil {
		log.Errorf("ошибка конфигурации: %s", err)
	}
	conn, err := postgres.New(postgres.Settings{
		Host:     c.PostgresConfig.Host,
		Port:     c.PostgresConfig.Port,
		Database: c.PostgresConfig.Name,
		User:     c.PostgresConfig.User,
		Password: c.PostgresConfig.Password,
		SSLMode:  c.PostgresConfig.SSLMode,
	})
	if err != nil {
		return nil, func() {}, timeWait, err
	}
	closer := func() {
		defer conn.Pool.Close()
	}

	repoStorage, err := repositoryStorage.New(conn.Pool, repositoryStorage.Options{}, log, repositoryStorage.Migration{})
	if err != nil {
		log.Fatalf("ошибка при создании postgres pool: %s", err)
	}
	return repoStorage, closer, timeWait, nil
}

func TestAccountExist(t *testing.T) {
	ctx := context.Background()

	client, closer, _ := Server(ctx)
	defer closer()

	type expectation struct {
		out *pb.ResponseTx
		err error
	}

	list, err := client.GetBalancesList(context.Background(), &empty.Empty{})
	if err != nil {
		t.Errorf("should get the getMaxMoney %s", err)
	}
	//create min account if not exist and update amount
	if _, ok := list.Balances[mintAddress]; !ok {
		tx, err := client.NewAccount(context.Background(), &pb.NewAccRequest{Address: mintAddr.Address})
		if err != nil {
			t.Fatalf("cannot create mint account tx %s error: %s", tx.String(), err)
		}
	}
	//create receiver accounts if not exist and update amount
	for i, v := range testAddrs {
		acc, ok := list.Balances[v.Address]
		if !ok {
			tx, err := client.NewAccount(context.Background(), &pb.NewAccRequest{Address: v.Address})
			if err != nil {
				t.Fatalf("cannot create reciver account %s tx %s error: %s", v.Address, tx.String(), err)
			}
			continue
		}
		testAddrs[i].Amount = acc.Val
	}
}

func TestSetMaxMoney(t *testing.T) {
	ctx := context.Background()

	client, closer, _ := Server(ctx)
	defer closer()

	type expectation struct {
		out *pb.ResponseTx
		err error
	}

	maxMoney, err := client.GetMaxMoney(context.Background(), &empty.Empty{})
	if err != nil {
		t.Errorf("should get the getMaxMoney %s", err)
	}

	failAmount := maxMoney.Amount - 1
	successAmount := maxMoney.Amount + 1

	tests := map[string]struct {
		in       *pb.SetMaxMoneyRequest
		expected expectation
	}{
		"Is_Amount_Less_Max_Money": {
			in: &pb.SetMaxMoneyRequest{
				Amount: failAmount,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: the new amount must be greater than the current amount of the max money field"),
			},
		},
		"Is_Amount_Greater_Max_Money": {
			in: &pb.SetMaxMoneyRequest{
				Amount: successAmount,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: fmt.Sprintf("успешно изменено с %d на %d", maxMoney.Amount, successAmount)},
				err: errors.New(""),
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.SetMaxMoney(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Msg != out.Msg {
					t.Errorf("Out -> \nWant: %q;\nGot: %q", tt.expected.out.Msg, out.Msg)
				} else {
					jsonb := pgtype.JSONB{}
					jsonb.Set(event.NewMaxMoney{
						Amount: big.NewInt(int64(tt.in.Amount)),
					})
					EventLogDbs = append(EventLogDbs, EventLogDb{
						TxHash:    out.TxHash,
						EventType: "NewMaxMoney",
						EventData: jsonb,
					})
				}
			}
		})
	}

}

func TestSetMint(t *testing.T) {
	ctx := context.Background()

	client, closer, _ := Server(ctx)
	defer closer()

	type expectation struct {
		out *pb.ResponseTx
		err error
	}

	tests := map[string]struct {
		in       *pb.MintRequest
		expected expectation
	}{
		"Is_Amount_Equal_Zero": {
			in: &pb.MintRequest{
				Amount:  0,
				Address: mintAddr.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: the amount must be greater than zero"),
			},
		},

		"Is_Amount_Greater_Zero": {
			in: &pb.MintRequest{
				Amount:  20000,
				Address: mintAddr.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: fmt.Sprintf("баланс адреса владельца успешно обновлен")},
				err: errors.New(""),
			},
		},

		"Is_Address_Not_Found": {
			in: &pb.MintRequest{
				Amount:  2000,
				Address: mintAddr.Address + converter.RandomString(15),
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: address wallet is not found"),
			},
		},
		"Is_Address_Length_More_36_Symbol": {
			in: &pb.MintRequest{
				Amount:  2000,
				Address: converter.RandomString(37),
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: address wallet is not found"),
			},
		},
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.Mint(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Msg != out.Msg {
					t.Errorf("Out -> \nWant: %q;\nGot: %q", tt.expected.out.Msg, out.Msg)
				} else {
					jsonb := pgtype.JSONB{}
					jsonb.Set(event.Mint{
						To:     tt.in.Address,
						Amount: big.NewInt(int64(tt.in.Amount)),
					})
					EventLogDbs = append(EventLogDbs, EventLogDb{
						TxHash:    out.TxHash,
						EventType: "Mint",
						EventData: jsonb,
					})
				}
			}
		})
	}
}

func TestSend(t *testing.T) {
	ctx := context.Background()

	client, closer, _ := Server(ctx)
	defer closer()

	maxMoney, err := client.GetMaxMoney(context.Background(), &empty.Empty{})
	if err != nil {
		t.Errorf("should get the getMaxMoney %s", err)
	}

	type expectation struct {
		out *pb.ResponseTx
		err error
	}

	type TestSend struct {
		in       *pb.SendRequest
		expected expectation
	}

	tests := map[string]TestSend{}

	for _, v := range testAddrs {
		tests[fmt.Sprintf("Is_Amount_Equal_Zero_Account_%s", v.Address)] = TestSend{
			in: &pb.SendRequest{
				Amount: 0,
				From:   mintAddr.Address,
				To:     v.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: the amount must be greater than zero"),
			},
		}

		tests[fmt.Sprintf("Is_Amount_Greater_Zero_%s", v.Address)] = TestSend{
			in: &pb.SendRequest{
				Amount: addAmountCycle,
				From:   mintAddr.Address,
				To:     v.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: fmt.Sprintf("баланс адреса %s увеличен на %d", v.Address, addAmountCycle)},
				err: errors.New(""),
			},
		}

		tests[fmt.Sprintf("Is_Amount_Greater_Max_Money_%s", v.Address)] = TestSend{
			in: &pb.SendRequest{
				Amount: maxMoney.Amount - 1,
				From:   mintAddr.Address,
				To:     v.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: fmt.Sprintf("баланс адреса %s увеличен на %d", v.Address, maxMoney.Amount-1)},
				err: errors.New(""),
			},
		}

		tests[fmt.Sprintf("Is_Amount_Less_Max_Money_%s", v.Address)] = TestSend{
			in: &pb.SendRequest{
				Amount: maxMoney.Amount + 1,
				From:   mintAddr.Address,
				To:     v.Address,
			},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: the amount must be less than maxMoney"),
			},
		}
	}

	for scenario, tt := range tests {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.Send(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Msg != out.Msg {
					t.Errorf("Out -> \nWant: %q;\nGot: %q", tt.expected.out.Msg, out.Msg)
				} else {
					jsonb := pgtype.JSONB{}
					jsonb.Set(event.LogSend{
						From:   tt.in.From,
						To:     tt.in.To,
						Amount: big.NewInt(int64(tt.in.Amount)),
					})
					EventLogDbs = append(EventLogDbs, EventLogDb{
						TxHash:    out.TxHash,
						EventType: "Sent",
						EventData: jsonb,
					})
				}
			}
		})
	}

	mintBalance, err := client.Balance(context.Background(), &pb.BalanceRequest{Address: mintAddr.Address})
	if err != nil {
		t.Errorf("should get the balance mint address %s", err)
	}

	mintTest := map[string]TestSend{
		"Amount_Greater_Than_Mint_Balance": {in: &pb.SendRequest{
			Amount: mintBalance.Amount + 1,
			From:   mintAddr.Address,
			To:     testAddrs[0].Address,
		},
			expected: expectation{
				out: &pb.ResponseTx{Msg: ""},
				err: errors.New("rpc error: code = Unknown desc = execution reverted: the amount must be less than the sender's balance"),
			},
		},
	}

	for scenario, tt := range mintTest {
		t.Run(scenario, func(t *testing.T) {
			out, err := client.Send(ctx, tt.in)
			if err != nil {
				if tt.expected.err.Error() != err.Error() {
					t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
				}
			} else {
				if tt.expected.out.Msg != out.Msg {
					t.Errorf("Out -> \nWant: %q;\nGot: %q", tt.expected.out.Msg, out.Msg)
				}
			}
		})
	}

}

func TestEventLogDb(t *testing.T) {
	if len(EventLogDbs) == 0 {
		t.Fatalf("EventLogDbs is empty")
		return
	}
	ctx := context.Background()
	clientDB, closer, timeWait, err := ServerDb(ctx)
	if timeWait.Seconds() > 120 {
		t.Errorf("timeWait is very long time: %f seconds", timeWait.Seconds())
	}
	fmt.Printf("waiting %f seconds\n", timeWait.Seconds())
	if err != nil {
		t.Errorf("ServerDb  %s\n", err)
	}
	defer closer()
	ch, cancel := context.WithTimeout(ctx, timeWait)
	defer cancel()
	select {
	case <-ch.Done():
		for _, v := range EventLogDbs {
			errW := fmt.Sprintf("txHash %s ", v.TxHash)
			params := event_log.EventLogParams{}
			params.SetTxHash(v.TxHash)
			params.SetEventType(v.EventType)
			params.SetEventData(v.EventData)
			_, err := clientDB.ReadEventLogByParam(ctx, params)
			if err != nil {
				t.Errorf("%s not found in db %s", errW, err)
				continue
			}
			//item, err := clientDB.ReadEventLogByTxHash(ctx, v.TxHash)
			//if err != nil {
			//	t.Errorf("%s not found in db %s", errW, err)
			//	continue
			//}
			//if item.EventType() != v.EventType {
			//	t.Errorf("%s EventType not equal %s != %s", errW, item.EventType(), v.EventType)
			//}
			//eq := reflect.DeepEqual(v.EventData.Get(), item.EventData().Get())
			//if !eq {
			//	t.Errorf("EventType Get() not equal %v != %v", v.EventData.Get(), item.EventData().Get())
			//}
		}
	}
}
