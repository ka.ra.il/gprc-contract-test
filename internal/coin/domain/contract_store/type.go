package contract_store

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
)

const (
	SUCCESS = "SUCCESS"
	FAILED  = "FAILED"
)

type Store struct {
	Client          *ethclient.Client
	StoreSession    *coin.StorageSession
	ContractAddress common.Address
	UrlHttp         string
	UrlWs           string
}
