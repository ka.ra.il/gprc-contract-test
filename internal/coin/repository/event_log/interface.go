package event_log

import (
	"context"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type EventLog interface {
	UpdateEventLogCountByFilters(ctx context.Context, tx pgx.Tx, ID uuid.UUID) error
}
