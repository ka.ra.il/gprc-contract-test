package postgres

import (
	"context"
	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	dao2 "gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/kafka_save_topic/postgres/dao"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/transaction"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/columnCode"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
	"strings"
)

var mappingSortKafka = map[columnCode.ColumnCode]string{
	"createdat": "createdat",
	"topic":     "topic",
}

func (r *Repository) CreateKafkaSaveTopic(ctx context.Context, msg *kafka_save_topic.KafkaSaveTopic) (*kafka_save_topic.KafkaSaveTopic, error) {
	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	queryInsert := r.genSQL.Insert("kafka_save_topic").
		Columns(strings.Join(dao2.CreateColumnEventLog, ","))
	queryInsert = queryInsert.Values(
		msg.Topic(),
		msg.EventData(),
		msg.CreatedAt(),
	)
	query, args, err := queryInsert.Suffix("ON CONFLICT DO NOTHING").ToSql()
	if err != nil {
		return nil, err
	}

	if _, errEx := tx.Exec(ctx, query, args...); errEx != nil {
		return nil, errEx
	}
	return msg, nil
}

func (r *Repository) ListKafkaSaveTopic(ctx context.Context, parameter queryParameter.QueryParameter) ([]*kafka_save_topic.KafkaSaveTopic, error) {

	tx, err := r.db.Begin(ctx)
	if err != nil {
		return nil, err
	}

	defer func(ctx context.Context, t pgx.Tx) {
		err = transaction.Finish(ctx, t, err)
	}(ctx, tx)

	response, err := r.listKafkaSaveTopic(ctx, tx, parameter)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (r *Repository) listKafkaSaveTopic(ctx context.Context, tx pgx.Tx, parameter queryParameter.QueryParameter) ([]*kafka_save_topic.KafkaSaveTopic, error) {
	var result []*kafka_save_topic.KafkaSaveTopic

	var builder = r.genSQL.Select(strings.Join(dao2.CreateColumnEventLog, ",")).
		From("kafka_save_topic")

	if len(parameter.Sorts) > 0 {
		builder = builder.OrderBy(parameter.Sorts.Parsing(mappingSortKafka)...)
	} else {
		builder = builder.OrderBy("createdat DESC")
	}

	if parameter.Pagination.Limit > 0 {
		builder = builder.Limit(parameter.Pagination.Limit)
	}
	if parameter.Pagination.Offset > 0 {
		builder = builder.Offset(parameter.Pagination.Offset)
	}

	query, args, err := builder.ToSql()
	rows, err := tx.Query(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	var items []dao2.KafkaSaveTopic
	if err = pgxscan.ScanAll(&items, rows); err != nil {
		return nil, err
	}

	for _, g := range items {
		domainEventLog, err := g.ToDomainEventLog()
		if err != nil {
			return nil, err
		}
		result = append(result, domainEventLog)
	}
	return result, nil
}
