package producer

import (
	"encoding/json"
	"fmt"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-kafka/v2/pkg/kafka"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
)

type Producer struct {
	Publisher *kafka.Publisher
	Topic     string
	Log       *logrus.Entry
}

type ConfigKafka struct {
	Topic   string
	Brokers []string
}

func New(conf ConfigKafka) (Producer, error) {
	logger := watermill.NewStdLogger(false, false)
	logger.Info("Starting the producer by ", watermill.LogFields{})
	publisher, err := kafka.NewPublisher(
		kafka.PublisherConfig{
			Brokers:   conf.Brokers,
			Marshaler: kafka.DefaultMarshaler{},
		},
		logger,
	)
	if err != nil {
		return Producer{}, err
	}
	return Producer{
		Publisher: publisher,
		Topic:     conf.Topic,
	}, nil
}

func (p *Producer) Send(payload []byte) error {
	msg := message.NewMessage(watermill.NewUUID(), payload)
	middleware.SetCorrelationID(watermill.NewShortUUID(), msg)
	err := p.Publisher.Publish(p.Topic, msg)
	if err != nil {
		return fmt.Errorf("cannot publish message %s", err)

	}
	return nil
}

func (p Producer) BatchSend(list []*event_log.EventLog) {
	for _, event := range list {
		payload, err := json.Marshal(*event)
		if err != nil {
			p.Log.Errorf("can not unmarshal event for kafka %s", err)
			continue
		}
		if len(payload) == 0 {
			continue
		}
		err = p.Send(payload)
		if err != nil {
			p.Log.Errorf("producer send: %s", err)
		}
	}
}
