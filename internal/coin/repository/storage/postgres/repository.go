package postgres

import (
	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/sirupsen/logrus"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/event_log"
	"go.uber.org/zap"
	"time"

	"github.com/pressly/goose"
)

type Repository struct {
	db      *pgxpool.Pool
	genSQL  squirrel.StatementBuilderType
	options Options
	log     *logrus.Entry
}

type Options struct {
	Timeout       time.Duration
	DefaultLimit  uint64
	DefaultOffset uint64
}

type Migration struct {
	Dir       string
	IsMigrate bool
}

func New(db *pgxpool.Pool, o Options, log *logrus.Entry, m Migration) (*Repository, error) {
	if err := migrations(db, log, m); err != nil {
		return nil, err
	}

	var r = &Repository{
		genSQL: squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		db:     db,
		log:    log,
	}

	r.SetOptions(o)
	return r, nil
}

func (r *Repository) SetOptions(options Options) {
	if options.DefaultLimit == 0 {
		options.DefaultLimit = 10
		r.log.Debug("set default options.DefaultLimit", zap.Any("defaultLimit", options.DefaultLimit))
	}

	if options.Timeout == 0 {
		options.Timeout = time.Second * 30
		r.log.Debug("set default options.Timeout", zap.Any("timeout", options.Timeout))
	}

	if r.options != options {
		r.options = options
		r.log.Info("set new options", zap.Any("options", r.options))
	}
}

func migrations(pool *pgxpool.Pool, log *logrus.Entry, m Migration) (err error) {
	if !m.IsMigrate {
		return nil
	}
	db, err := goose.OpenDBWithDriver("postgres", pool.Config().ConnConfig.ConnString())
	if err != nil {
		log.Error(err)
		return err
	}
	defer func() {
		if errClose := db.Close(); errClose != nil {
			log.Error(errClose)
			err = errClose
			return
		}
	}()

	goose.SetTableName("db_version")
	if err = goose.Run("up", db, m.Dir); err != nil {
		log.Error(err, zap.String("command", "up"))
		return err
	}
	return
}

func (r Repository) toCopyFromSource(events ...*event_log.EventLog) pgx.CopyFromSource {
	rows := make([][]interface{}, len(events))

	for i, val := range events {
		rows[i] = []interface{}{
			val.Address(),
			val.Topics(),
			val.Data(),
			val.BlockNumber(),
			val.TxHash(),
			val.TxIndex(),
			val.BlockHash(),
			val.Index(),
			val.Removed(),
			val.CreatedAt(),
			val.ModifiedAt(),
		}
	}
	return pgx.CopyFromRows(rows)
}
