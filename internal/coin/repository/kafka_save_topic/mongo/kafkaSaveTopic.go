package mongo

import (
	"context"
	"encoding/json"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/domain/kafka_save_topic"
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/repository/kafka_save_topic/mongo/dao"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/type/queryParameter"
)

func (r *Repository) CreateKafkaSaveTopic(ctx context.Context, msg *kafka_save_topic.KafkaSaveTopic) (*kafka_save_topic.KafkaSaveTopic, error) {
	data := make(map[string]interface{}, 0)
	err := json.Unmarshal(msg.EventData().Bytes, &data)
	if err != nil {
		r.log.Infof("json.Unmarshal %s", err)
		return nil, err
	}
	item, err := dao.New(msg.Topic(), data, msg.CreatedAt())
	if err != nil {
		r.log.Infof("dao.New %s", err)
		return nil, err
	}
	_, err = r.db.Collection("kafkaSaveTopic").InsertOne(ctx, item)
	if err != nil {
		r.log.Infof("save collection  kafkaSaveTopic %s", err)
		return nil, err
	}
	return msg, nil
}

func (r *Repository) ListKafkaSaveTopic(ctx context.Context, parameters queryParameter.QueryParameter) ([]*kafka_save_topic.KafkaSaveTopic, error) {
	//TODO implement me
	panic("implement me")
}
