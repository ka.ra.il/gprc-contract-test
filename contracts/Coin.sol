pragma solidity ^0.8.17;

import "./Ownable.sol";
import "./Strings.sol";

contract Coin is Ownable
{
    mapping (string => wallet) public wallets;
    mapping (uint256 => string) public walletOwner;
    string[] public addressIndices;
    bool isSendProcessing;
    uint256 private seed;
    uint private maxMoney;


    struct wallet {
        uint256 id;
        uint val;
        bool isValue;
    }

    event NewMaxMoney(uint maxMoney);
    event Mint(string to, uint amount);
    event Sent(string from, string to, uint amount);
    event NewAccount(string to, uint amount);
    event ChangeFlagProcessing(bool flagStatus);

    constructor() {
        maxMoney = 1000;
        isSendProcessing = true;
        seed = (block.timestamp + block.difficulty) % 100;
    }

    function setMaxMoney(uint amount) public onlyOwner {
        require(amount > maxMoney, "the new amount must be greater than the current amount of the max money field");
        maxMoney = amount;
        emit NewMaxMoney(maxMoney);
    }

    function getMaxMoney()  public view returns (uint){
        return maxMoney;
    }

    function setStatusProcessing(bool status)  public onlyOwner returns (bool){
        require(isSendProcessing != status, "the isSendProcessing has not changed because the current value is equal to the one being changed");
        isSendProcessing = status;
        emit ChangeFlagProcessing(status);
        return isSendProcessing;
    }

    function mint(string memory addr, uint amount) public onlyOwner statusMoneyProcessing {
        require(wallets[addr].isValue == true, "address wallet is not found");
        sendBalance(addr, amount);
        emit Mint(addr, amount);

    }

    function send(string memory from, string memory to, uint amount) public onlyOwner statusMoneyProcessing {
        require(amount < wallets[from].val, "the amount must be less than the sender's balance");
        require(maxMoney > amount, "the amount must be less than maxMoney");
        sendBalance(to, amount);
        wallets[from].val -= amount;
        emit Sent(from, to, amount);
    }

    function newAccount(string calldata addr, uint amount) public onlyOwner statusMoneyProcessing {
        require(validateAddress(addr), "new addr is not valid");
        require(wallets[addr].isValue == false, "wallet address is exists");
        wallets[addr] = createWallet(addr, amount);
        emit NewAccount(addr, amount);
    }

    function getBalancesList() public view returns (string[] memory, uint[] memory, uint[] memory) {
        uint arrayLength = addressIndices.length;
        string[] memory addresses = new string[](arrayLength);
        uint[] memory values = new uint[](arrayLength);
        uint[] memory ids = new uint[](arrayLength);

        for (uint i=0; i<arrayLength; i++) {
            addresses[i] = addressIndices[i];
            values[i] = wallets[addressIndices[i]].val;
            ids[i] = wallets[addressIndices[i]].id;
        }
        return (addresses, values, ids);
    }

    function getFields() public view returns(uint, bool, uint, uint256) {
        return (maxMoney, isSendProcessing, addressIndices.length, seed);
    }

    function createWallet(string memory addr, uint amount) private returns (wallet memory) {
        addressIndices.push(addr);
        uint256 randomWal = getRandomNumber();
        walletOwner[randomWal] = addr;
        return wallet(randomWal, amount, true);
    }

    function sendBalance(string memory receiver, uint amount) private {
        require(amount > 0, "the amount must be greater than zero");
        require(wallets[receiver].isValue == true, "wallet address is not found");
        wallets[receiver].val += amount;
    }

    function getRandomNumber() public returns (uint256) {
        seed = (seed + block.timestamp + block.difficulty);
        return seed;
    }

    function validateAddress(string memory addr) private pure returns(bool) {
        bytes memory b = bytes(addr);
        if ((b.length == 0) || (b.length > 36))  return false;
        return true;
    }

    modifier statusMoneyProcessing() {
        require(isSendProcessing == true, "the isSendProcessing equal false");
        _;
    }

}