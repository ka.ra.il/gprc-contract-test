package postgres

import (
	"gitlab.com/ka.ra.il/grpc-contract-test/internal/coin/useCase/adapters/storage"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Repository struct {
	db     *pgxpool.Pool
	genSQL squirrel.StatementBuilderType

	repoEventLog storage.EventLog

	options Options
}

type Options struct {
	DefaultLimit  uint64
	DefaultOffset uint64
}

func New(db *pgxpool.Pool, repoEventLog storage.EventLog, o Options, migrationDir string) (*Repository, error) {

	var r = &Repository{
		genSQL:       squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar),
		repoEventLog: repoEventLog,
		db:           db,
	}

	r.SetOptions(o)
	return r, nil
}

func (r *Repository) SetOptions(options Options) {
	if options.DefaultLimit == 0 {
		options.DefaultLimit = 10
	}

	if r.options != options {
		r.options = options
	}
}
