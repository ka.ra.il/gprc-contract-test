-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS kafka_save_topic
(
    topic       VARCHAR(128),
    data        JSONB NOT NULL DEFAULT '{}'::jsonb,
    createdAt   timestamp
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS kafka_save_topic;
-- +goose StatementEnd
