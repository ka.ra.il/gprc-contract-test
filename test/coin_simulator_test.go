package coin_test

import (
	"context"
	"errors"
	"github.com/ardanlabs/ethereum"
	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/ka.ra.il/grpc-contract-test/contracts/coin"
	"gitlab.com/ka.ra.il/grpc-contract-test/pkg/helper/converter"
	"math/big"
	"strconv"
	"testing"
	//"github.com/ethereum/go-ethereum/common"
	_ "github.com/golang/mock/mockgen/model"
)

const (
	ownerDeployerAcc = 0
	initialMaxMoney  = 1000
	initialCnt       = 0
	ownerWallet      = 0
	addAmountCycle   = 10
)

func TestCoin(t *testing.T) {
	ctx := context.Background()
	numAccounts := 1
	backend, err := ethereum.CreateSimulatedBackend(numAccounts, true, big.NewInt(100))
	if err != nil {
		t.Fatalf("unable to create simulated backend: %s", err)
	}
	defer backend.Close()

	// =========================================================================

	ownerDeployer, err := ethereum.NewClient(backend, backend.PrivateKeys[ownerDeployerAcc])
	if err != nil {
		t.Fatalf("unable to create ownerDeployerAcc: %s", err)
	}

	type Receiver struct {
		Amount  *big.Int
		Address string
	}

	cntReceivers := 5
	receivers := make([]Receiver, 0)
	for id := 0; id < cntReceivers; id++ {
		idStr := strconv.Itoa(id)
		receivers = append(receivers, Receiver{
			Amount:  big.NewInt(int64(id * 100)),
			Address: idStr,
		})
	}
	cntInitialAddr := cntReceivers + 1

	var ownerFirstMintAmount uint64 = 900

	callOpts, err := ownerDeployer.NewCallOpts(ctx)
	if err != nil {
		t.Fatalf("unable to create call opts: %s", err)
	}

	// =========================================================================

	const gasLimit = 25000000
	const valueGwei = 0.0

	var testCoin *coin.Storage
	var sendSum uint64

	type expectation struct {
		out common.Hash
		err error
	}
	type walletParams struct {
		Address string
		Amount  *big.Int
	}

	type testItem struct {
		in       walletParams
		expected expectation
	}

	// =========================================================================

	t.Run("deploy coin", func(t *testing.T) {
		deployTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for deploy: %s", err)
		}

		contractID, tx, _, err := coin.DeployStorage(deployTranOpts, ownerDeployer.Backend)
		if err != nil {
			t.Fatalf("unable to deploy bank: %s", err)
		}

		if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
			t.Fatalf("waiting for deploy: %s", err)
		}

		testCoin, err = coin.NewStorage(contractID, ownerDeployer.Backend)
		if err != nil {
			t.Fatalf("unable to create a bank: %s", err)
		}
	})

	// =========================================================================

	t.Run("check owner matches", func(t *testing.T) {
		owner, err := testCoin.Owner(callOpts)
		if err != nil {
			t.Fatalf("unable to get account owner: %s", err)
		}
		if owner != ownerDeployer.Address() {
			t.Fatalf("Retrieved owner doesn't match expectations: %v != %v", owner, ownerDeployer.Address())
		}
	})

	// =========================================================================

	t.Run("check initial fields ", func(t *testing.T) {
		initialBalance, err := testCoin.Wallets(callOpts, receivers[ownerWallet].Address)
		if err != nil {
			t.Fatalf("should get the initial balance: %s", err)
		}
		if initialBalance.Val.Uint64() != 0 {
			t.Fatal("initialBalance should be 0")
		}

		maxMoney, isSendProcessing, addressIndicesLength, _, err := testCoin.GetFields(callOpts)
		if err != nil {
			t.Fatalf("should get the get fields: %s", err)
		}

		if maxMoney.Int64() != initialMaxMoney {
			t.Fatalf("maxMoney should be equal 100")
		}

		if isSendProcessing != true {
			t.Fatalf("isSendProcessing should be equal true")
		}

		if addressIndicesLength.Uint64() != uint64(initialCnt) {
			t.Fatalf("addressIndicesLength should be equal 0")
		}
	})

	// =========================================================================

	t.Run("check initial balance list ", func(t *testing.T) {
		addresses, values, ids, err := testCoin.GetBalancesList(callOpts)

		if err != nil {
			t.Fatalf("should get the initial balance list: %s", err)
		}
		if len(addresses) != initialCnt {
			t.Fatal("initial addresses length should be 0")
		}

		if len(values) != initialCnt {
			t.Fatal("initial values length should be 0")
		}

		if len(ids) != initialCnt {
			t.Fatal("initial ids length should be 0")
		}
	})

	t.Run("create new account", func(t *testing.T) {
		for _, v := range receivers {
			sendTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
			if err != nil {
				t.Fatalf("unable to create transaction opts for sen: %s", err)
			}
			tx, err := testCoin.NewAccount(sendTranOpts, v.Address, v.Amount)
			if err != nil {
				t.Fatalf("should be able to send coin: %s", err)
			}
			if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
				t.Fatalf("should be able to send: %s", err)
			}

			currentBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the current balance: %s", err)
			}
			if currentBalance.Val.Uint64() != v.Amount.Uint64() {
				t.Fatalf("new account address %s should be %d", v.Address, v.Amount)
			}
		}
	})

	// =========================================================================

	t.Run("check mint method", func(t *testing.T) {
		mintTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for deposit: %s", err)
		}

		amount := big.NewInt(int64(ownerFirstMintAmount))
		tx, err := testCoin.Mint(mintTranOpts, receivers[ownerWallet].Address, amount)
		if err != nil {
			t.Fatalf("should be able to mint coin: %s", err)
		}
		if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
			t.Fatalf("should be able to mint coin: %s", err)
		}

		currentBalance, err := testCoin.Wallets(callOpts, receivers[ownerWallet].Address)
		if err != nil {
			t.Fatalf("should get the current balance: %s", err)
		}
		if currentBalance.Val.Uint64() != amount.Uint64() {
			t.Fatalf("currentBalance not equal amount: %s", err)
		}

		tests := map[string]struct {
			in       walletParams
			expected expectation
		}{
			"Amount_Is_Zero": {
				in: walletParams{
					Address: receivers[ownerWallet].Address,
					Amount:  big.NewInt(0),
				},
				expected: expectation{
					err: errors.New("extracting tx error: execution reverted: the amount must be greater than zero"),
				},
			},
			"Address_Is_Not_Exist": {
				in: walletParams{
					Address: converter.RandomString(30),
					Amount:  big.NewInt(addAmountCycle),
				},
				expected: expectation{
					err: errors.New("extracting tx error: execution reverted: address wallet is not found"),
				},
			},
		}

		for scenario, tt := range tests {
			t.Run(scenario, func(t *testing.T) {
				TranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
				if err != nil {
					t.Errorf("unable to create transaction opts for mint validation: %s", err)
				}
				tx, err = testCoin.Mint(TranOpts, tt.in.Address, tt.in.Amount)
				if err != nil {
					t.Errorf("should be able to mint function %s", err)
				}

				_, err = ownerDeployer.WaitMined(ctx, tx)
				if err != nil {
					if tt.expected.err.Error() != err.Error() {
						t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
					}
				} else {
					t.Errorf("shoul be some error")
				}

			})
		}
	})

	// =========================================================================

	t.Run("check send method", func(t *testing.T) {

		tests := make(map[string]testItem, 0)
		var i int
		for _, v := range receivers[1:] {
			tests["Amount_Is_Zero_"+v.Address] = testItem{
				in: walletParams{
					Address: v.Address,
					Amount:  big.NewInt(0),
				},
				expected: expectation{
					err: errors.New("extracting tx error: execution reverted: the amount must be greater than zero"),
				},
			}

			tests["Address_Is_Not_Exist_"+v.Address] = testItem{
				in: walletParams{
					Address: converter.RandomString(30),
					Amount:  big.NewInt(addAmountCycle),
				},
				expected: expectation{
					err: errors.New("extracting tx error: execution reverted: address wallet is not found"),
				},
			}
			i++

			if v.Amount.Uint64() == 0 {
				continue
			}
			initialBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the initial balance: %s", err)
			}
			if !initialBalance.IsValue {
				t.Fatal("initialBalance is not correct for receiver address")
			}

			sendTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
			if err != nil {
				t.Fatalf("unable to create transaction opts for sen: %s", err)
			}
			addAmount := big.NewInt(int64(addAmountCycle))
			tx, err := testCoin.Send(sendTranOpts, receivers[ownerWallet].Address, v.Address, addAmount)
			if err != nil {
				t.Fatalf("should be able to send: %s", err)
			}
			if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
				t.Fatalf("should be able to send: %s", err)
			}
			sendSum += addAmount.Uint64()
			if err != nil {
				t.Fatalf("should be able to send coin: %s", err)
			}

			currentBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the current balance: %s", err)
			}
			if currentBalance.Val.Uint64() != (v.Amount.Uint64() + addAmount.Uint64()) {
				t.Fatalf("currentBalance not equal amount: %s", err)
			}
		}

		for scenario, tt := range tests {
			t.Run(scenario, func(t *testing.T) {
				TranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
				if err != nil {
					t.Errorf("unable to create transaction opts for send validation: %s", err)
				}
				tx, err := testCoin.Mint(TranOpts, tt.in.Address, tt.in.Amount)
				if err != nil {
					t.Errorf("should be able to mint function %s", err)
				}

				_, err = ownerDeployer.WaitMined(ctx, tx)
				if err != nil {
					if tt.expected.err.Error() != err.Error() {
						t.Errorf("Err -> Want: \n%q\n;Got: \n%q\n", tt.expected.err, err)
					}
				} else {
					t.Errorf("shoul be some error")
				}

			})
		}
	})

	// =========================================================================

	t.Run("check balance list count", func(t *testing.T) {
		cntAddr := len(receivers)
		addresses, values, ids, err := testCoin.GetBalancesList(callOpts)
		if err != nil {
			t.Fatalf("should get balance list: %s", err)
		}
		if len(addresses) != cntAddr {
			t.Fatalf("initial addresses length should be %d", cntInitialAddr)
		}

		if len(values) != cntAddr {
			t.Fatalf("initial values length should be %d", cntInitialAddr)
		}

		if len(ids) != cntAddr {
			t.Fatalf("initial ids length should be %d", cntInitialAddr)
		}
	})

	// =========================================================================

	t.Run("amount exceeding the maxMoney", func(t *testing.T) {
		currentMaxMoney, err := testCoin.GetMaxMoney(callOpts)
		if err != nil {
			t.Fatalf("should get the current get max money: %s", err)
		}
		for _, v := range receivers[1:] {
			sendTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
			if err != nil {
				t.Fatalf("unable to create transaction opts for sen: %s", err)
			}

			addAmount := v.Amount.Uint64() + currentMaxMoney.Uint64()
			tx, err := testCoin.Send(sendTranOpts, receivers[ownerWallet].Address, v.Address, big.NewInt(int64(addAmount)))
			if err != nil {
				t.Fatalf("should be able to send coin: %s", err)
			}
			if _, err := ownerDeployer.WaitMined(ctx, tx); err == nil {
				t.Fatal("should be not able to send: for amount exceeding the maxMoney")
			}

			currentBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the current balance: %s", err)
			}
			compareBalance := v.Amount.Uint64() + addAmountCycle
			if currentBalance.Val.Uint64() != compareBalance {
				t.Fatalf("currentBalance address %s should be %d amount", v.Address, compareBalance)
			}
		}
	})

	// =========================================================================

	t.Run("check change maxMoney", func(t *testing.T) {
		currentMaxMoney, err := testCoin.GetMaxMoney(callOpts)
		if err != nil {
			t.Fatalf("should get the current get max money: %s", err)
		}
		failAmount := big.NewInt(initialMaxMoney - 1)
		tranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for SetMaxMoney fail: %s", err)
		}
		tx, err := testCoin.SetMaxMoney(tranOpts, failAmount)
		if err != nil {
			t.Fatalf("should get the current get max money: %s", err)
		}

		receipt, err := backend.TransactionReceipt(context.Background(), tx.Hash())
		if err != nil {
			t.Fatal(err)
		}
		if receipt.Status != 0 {
			t.Fatalf("receipt status for fail SetMaxMoney  shouldbe 0 tx: %s, amount: %d", tx.Hash().String(), failAmount)
		}

		currentMaxMoney2, err := testCoin.GetMaxMoney(callOpts)
		if err != nil {
			t.Fatalf("should get the current get max money 2: %s", err)
		}
		if currentMaxMoney.Uint64() != currentMaxMoney2.Uint64() {
			t.Fatal("currentMaxMoney should not be change not correct amount")
		}

		successAmount := big.NewInt(initialMaxMoney * 2)
		tranOpts, err = ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for SetMaxMoney success: %s", err)
		}
		tx, err = testCoin.SetMaxMoney(tranOpts, successAmount)
		if err != nil {
			t.Fatalf("should get the current get max money success: %s", err)
		}

		currentMaxMoney3, err := testCoin.GetMaxMoney(callOpts)
		if err != nil {
			t.Fatalf("should get the current get max money 2: %s", err)
		}
		if currentMaxMoney3.Uint64() != successAmount.Uint64() {
			t.Fatalf("currentMaxMoney should be equal: %d", successAmount.Uint64())
		}

	})

	// =========================================================================

	t.Run("check setStatusProcessing false", func(t *testing.T) {
		tranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for SetStatusProcessing : %s", err)
		}
		tx, err := testCoin.SetStatusProcessing(tranOpts, false)
		receipt, err := backend.TransactionReceipt(context.Background(), tx.Hash())
		if err != nil {
			t.Fatal(err)
		}
		if receipt.Status != 1 {
			t.Fatalf("receipt status for success setStatusProcessing  shouldbe 1 tx: %s, status: %t", tx.Hash().String(), false)
		}

		mintTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for mint : %s", err)
		}
		amount := big.NewInt(10000)
		tx, err = testCoin.Mint(mintTranOpts, receivers[ownerWallet].Address, amount)
		if err != nil {
			t.Fatalf("should be able to mint coin: %s", err)
		}

		if _, err := ownerDeployer.WaitMined(ctx, tx); err == nil {
			t.Fatal("should be not able to mint for setStatusProcessing=false")
		}

		currentBalance, err := testCoin.Wallets(callOpts, receivers[ownerWallet].Address)
		if err != nil {
			t.Fatalf("should get the current balance: %s", err)
		}
		if currentBalance.Val.Uint64() != (ownerFirstMintAmount - sendSum) {
			t.Fatalf("currentBalance not equal amount: %s", err)
		}

		for _, v := range receivers[1:] {
			sendTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
			if err != nil {
				t.Fatalf("unable to create transaction opts for sen: %s", err)
			}

			tx, err := testCoin.Send(sendTranOpts, receivers[ownerWallet].Address, v.Address, v.Amount)
			if err != nil {
				t.Fatalf("should be able to send coin: %s", err)
			}
			if _, err := ownerDeployer.WaitMined(ctx, tx); err == nil {
				t.Fatal("should be not able to mint setStatusProcessing=false")
			}

			receipt, err := backend.TransactionReceipt(context.Background(), tx.Hash())
			if err != nil {
				t.Fatal(err)
			}
			if receipt.Status != 0 {
				t.Fatalf("receipt status for send method should be 0 when isSendProcessing equal false,  tx: %s, address receiver: %s, amount: %d", tx.Hash().String(), v.Address, v.Amount)
			}

			currentBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the current balance: %s", err)
			}
			if currentBalance.Val.Uint64() != v.Amount.Uint64()+addAmountCycle {
				t.Fatalf("currentBalance not equal amount: %s", err)
			}
		}
	})

	// =========================================================================

	t.Run("check send method double amount", func(t *testing.T) {
		tranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
		if err != nil {
			t.Fatalf("unable to create transaction opts for SetStatusProcessing : %s", err)
		}
		tx, err := testCoin.SetStatusProcessing(tranOpts, true)
		if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
			t.Fatalf("should be able to  SetStatusProcessing %s", err)
		}

		for _, v := range receivers[1:] {
			startBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the initial balance: %s", err)
			}
			if startBalance.Val.Uint64() != v.Amount.Uint64()+addAmountCycle {
				t.Fatalf("currentBalance not equal amount: %s", err)
			}

			sendTranOpts, err := ownerDeployer.NewTransactOpts(ctx, gasLimit, big.NewFloat(valueGwei))
			if err != nil {
				t.Fatalf("unable to create transaction opts for sen: %s", err)
			}

			addAmount := big.NewInt(int64(addAmountCycle))
			tx, err := testCoin.Send(sendTranOpts, receivers[ownerWallet].Address, v.Address, addAmount)
			if err != nil {
				t.Fatalf("should be able to send coin: %s", err)
			}
			if _, err := ownerDeployer.WaitMined(ctx, tx); err != nil {
				t.Fatalf("should be able to send %s", err)
			}
			sendSum += addAmountCycle

			currentBalance, err := testCoin.Wallets(callOpts, v.Address)
			if err != nil {
				t.Fatalf("should get the current balance: %s", err)
			}
			if currentBalance.Val.Uint64() != v.Amount.Uint64()+2*addAmountCycle {
				t.Fatalf("currentBalance not equal amount: %s", err)
			}
			break
		}
	})

}
